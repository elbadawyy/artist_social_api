module MessageHelper
  require "base64"

  def save_profile_image(user_id,image_string,extension)
      image_path = "images/#{user_id}/profile_images/#{String(Time.now.to_i)}#{Random.rand(0...10000)}.#{extension}"
      dir = File.dirname("public/#{image_path}")
      FileUtils.mkdir_p(dir) unless File.directory?(dir)
      
      File.open("public/#{image_path}", 'wb') do |f|
        f.write(Base64.decode64(image_string))
      end

      image_path
  end

  def save_message_attachment(message,base64,extension)
    case message.message_type_id
      when 2    
        type_title="images"
      when 3    
        type_title="audios"
      when 4   
        type_title="videos"
      when 5   
        type_title="docs"
    end
    
    attachment_path = "message/#{message.id}/#{type_title}/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{extension}"
    dir = File.dirname("public/#{attachment_path}")
    FileUtils.mkdir_p(dir) unless File.directory?(dir)
      
    File.open("public/#{attachment_path}", 'wb') do |f|
      f.write(Base64.decode64(base64))
    end
    attachment=MessageAttachment.new(attachment_path: attachment_path)
    message.message_attachments << attachment
    message.save
  end

  def save_post_audio(post,base64)
    
  end

  def save_post_video(post,base64)

  end

end

