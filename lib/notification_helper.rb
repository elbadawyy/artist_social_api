require 'fcm'

module NotificationHelper
    def send_notification(tokens,title,message,notification)
        fcm = FCM.new('AAAAUxLMhHM:APA91bEmh9JNSzh0e9Yb8tmo7gERGmM9MO__6ytBeKrvAXQvX7tHtzzo5F0G2d1QhouCJqpeRFm5V-ObBeyd2bMy10S3Uv_J-U9-H9WmD6lmVKg_zf4sJCHLuSA7Des1H3QpW16zUcQN') # Find server_key on: your firebase console on web > tab general > web api key
        options = {
                priority: 10,
                notification: {
                    title: title,
                    body: message,
                    sound: "IsyncTone.wav",
                    from_user_info: notification.from_user_info,
                    type:notification.notification_type,
                    target_id:notification.target_id,
                    icon: "myicon"}
                }
        response = fcm.send(tokens, options)
        
        if(response)
            return true
        else
            return false
        end

    end

    def notify_user(to_user,title,body,from_user=nil,notification_type="from_posts",target_id=nil)

        tokens=UserFcmToken.where(user_id:to_user.id).map(&:fcm_token)
        from_user_id=from_user.present? ? from_user.id : nil
        notification=Notification.create(from_id:from_user_id,to_id:to_user.id,body:body,notification_type:notification_type,target_id:target_id)
        send_notification(tokens,title,body,notification)
        increase_unreaded(to_user.id)
    
    end

    def increase_unreaded(user_id)
        unreaded=UnreadedNotification.find_by_user_id(user_id)

        if unreaded 
            unreaded.unreaded_count = unreaded.unreaded_count + 1
            unreaded.save
        else
            UnreadedNotification.create(user_id:user_id,unreaded_count:1)
        end

    end
end

  