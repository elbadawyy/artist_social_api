module ProfileHelper
    require "base64"


    def save_profile_image(user_id,image_string,extension)
        image_path = "images/#{user_id}/profile_images/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{extension}"
        dir = File.dirname("public/#{image_path}")
        FileUtils.mkdir_p(dir) unless File.directory?(dir)
        
        File.open("public/#{image_path}", 'wb') do |f|
          f.write(Base64.decode64(image_string))
        end

        image_path
    end

    def save_cover_image(user_id,image_string,extension)
        image_path = "images/#{user_id}/cover_images/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{extension}"
        dir = File.dirname("public/#{image_path}")
        FileUtils.mkdir_p(dir) unless File.directory?(dir)
        
        File.open("public/#{image_path}", 'wb') do |f|
          f.write(Base64.decode64(image_string))
        end

        image_path
    end

    def save_power_of_wings_images(image_string,image_path)
        File.open("public/#{image_path}", 'wb') do |f|
          f.write(Base64.decode64(image_string))
        end
    end
  
  


end

