module EventHelper
    require "base64"


  def save_event_cover(event_id,image_string,ext)
    image_path = "images/events/#{event_id}/event_covers/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{ext}"
    dir = File.dirname("public/#{image_path}")
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    File.open("public/#{image_path}", 'wb') do |f|
      f.write(Base64.decode64(image_string))
    end
    image_path
      
  end


  


end

