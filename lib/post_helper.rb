module PostHelper
  require "base64"
  require 'libreconv'


  def save_profile_image(user_id,image_string,extension)
      image_path = "images/#{user_id}/profile_images/#{String(Time.now.to_i)}#{Random.rand(0...10000)}.#{extension}"
      dir = File.dirname("public/#{image_path}")
      FileUtils.mkdir_p(dir) unless File.directory?(dir)
      
      File.open("public/#{image_path}", 'wb') do |f|
        f.write(Base64.decode64(image_string))
      end

      image_path
  end

  def save_post_attachment(post,base64,extension)
    case post.post_type_id
      when 2    
        type_title="images"
      when 3    
        type_title="audios"
      when 4   
        type_title="videos"
      when 5   
        type_title="docs"
    end
    if post.post_type_id != 5
      attachment_path = "posts/#{post.id}/#{type_title}/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{extension}"
      dir = File.dirname("public/#{attachment_path}")
      FileUtils.mkdir_p(dir) unless File.directory?(dir)
        
      File.open("public/#{attachment_path}", 'wb') do |f|
        f.write(Base64.decode64(base64))
      end
  
    else
      attachment_path = get_pdf_path(post,base64)

    end
    attachment=PostAttachment.new(attachment_path: attachment_path)
    post.post_attachments << attachment
    post.save
  end

  def get_pdf_path(post,base64)
    tmp_attachment_path = "posts/#{post.id}/tmp/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}"
    tmp_attachment_path_dir =  File.dirname("public/#{tmp_attachment_path}")

    pdf_attatchment_path = "posts/#{post.id}/docs/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.pdf"
    pdf_attachment_path_dir =  File.dirname("public/#{pdf_attatchment_path}")

    FileUtils.mkdir_p(tmp_attachment_path_dir) unless File.directory?(tmp_attachment_path_dir)
    FileUtils.mkdir_p(pdf_attachment_path_dir) unless File.directory?(pdf_attachment_path_dir)

    File.open("public/#{tmp_attachment_path}", 'wb') do |f|
        f.write(Base64.decode64(base64))
    end
    
    
    Libreconv.convert( "public/#{tmp_attachment_path}" , "public/#{pdf_attatchment_path}" )
    return pdf_attatchment_path

  end

    def save_post_tag(post_id,post_tag_ids)
      if post_tag_ids.kind_of?(Array) && post_tag_ids.count > 0
        PostTag.where(:post_id => post_id ).destroy_all

        post_tag_ids.map { |user_id|  
          PostTag.create(post_id: post_id,user_id: user_id)
        }
      end
      
    end

    def save_post_hashtag(post_id,hashtags)
      if hashtags.kind_of?(Array) && hashtags.count > 0
        hashtags.map { |hashtag_string|  
          hashtag=Hashtag.find_by_hashtag_body(hashtag_string)
          if hashtag
            PostHashtag.create(post_id:post_id,hashtag_id:hashtag.id)
          else
            hashtag=Hashtag.create(hashtag_body: hashtag_string)
            PostHashtag.create(post_id:post_id,hashtag_id:hashtag.id)

          end
        }

      end
    end


end

