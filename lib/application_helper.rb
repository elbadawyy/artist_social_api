module ApplicationHelper
  require "base64"


  def save_cv_file(user_id,file_base64_string,ext)
    file_path = "cvs/#{user_id}/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{ext}"
    dir = File.dirname("public/#{file_path}")
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    File.open("public/#{file_path}", 'wb') do |f|
      f.write(Base64.decode64(file_base64_string))
    end
    file_path
      
  end


  


end

