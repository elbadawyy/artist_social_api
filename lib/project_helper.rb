module ProjectHelper
    require "base64"


    def save_project_cover(project,image_string,ext)
      image_path = "images/projects/#{project.id}/project_covers/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{ext}"
      dir = File.dirname("public/#{image_path}")
      FileUtils.mkdir_p(dir) unless File.directory?(dir)
      
      File.open("public/#{image_path}", 'wb') do |f|
        f.write(Base64.decode64(image_string))
      end
      project.cover_path = image_path
      project.save
      
  end

  def save_project_gallary(project,image_string_array)
    image_string_array.map { |string_obj|
      if (string_obj["image_type"] == "image")
        image_path = "images/projects/#{project.id}/project_gallary/images/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{string_obj["ext"]}"
      else
        image_path = "images/projects/#{project.id}/project_gallary/videos/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{string_obj["ext"]}"
      end

      dir = File.dirname("public/#{image_path}")
      FileUtils.mkdir_p(dir) unless File.directory?(dir)
      
      File.open("public/#{image_path}", 'wb') do |f|
        f.write(Base64.decode64(string_obj["base64"]))
      end

      ProjectImage.create(project_id: project.id,image_path: image_path,image_type:string_obj["image_type"])
    }
end

    def save_cover_image(user_id,image_string_arr,extension)
        image_path = "images/#{user_id}/cover_images/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{extension}"
        dir = File.dirname("public/#{image_path}")
        FileUtils.mkdir_p(dir) unless File.directory?(dir)
        
        File.open("public/#{image_path}", 'wb') do |f|
          f.write(Base64.decode64(image_string))
        end

        image_path
    end

    def save_power_of_wings_images(image_string,image_path)
        File.open("public/#{image_path}", 'wb') do |f|
          f.write(Base64.decode64(image_string))
        end
    end
  
  


end

