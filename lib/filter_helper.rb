module FilterHelper

    def get_date_range(start_date_param,end_date_param)
        date_range={}

		if start_date_param.present?
			start_date=Date.parse(start_date_param).beginning_of_day
		else
			start_date=Date.today.beginning_of_day
		end

		if end_date_param.present?
			end_date=Date.parse(end_date_param).end_of_day
		else
			end_date=Date.today.end_of_day
		end

        date_range=start_date .. end_date
        date_range

    end
end