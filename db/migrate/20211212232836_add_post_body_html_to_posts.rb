class AddPostBodyHtmlToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :post_body_html, :text
  end
end
