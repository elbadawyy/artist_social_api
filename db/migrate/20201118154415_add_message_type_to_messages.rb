class AddMessageTypeToMessages < ActiveRecord::Migration[5.2]
  def change
    add_reference :messages, :message_type, foreign_key: true,default: 1
  end
end
