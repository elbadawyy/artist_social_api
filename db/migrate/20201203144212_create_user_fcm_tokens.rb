class CreateUserFcmTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :user_fcm_tokens do |t|
      t.references :user, foreign_key: true
      t.string :fcm_token

      t.timestamps
    end
  end
end
