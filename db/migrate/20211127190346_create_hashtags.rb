class CreateHashtags < ActiveRecord::Migration[5.2]
  def change
    create_table :hashtags do |t|
      t.text :hashtag_body

      t.timestamps
    end
  end
end
