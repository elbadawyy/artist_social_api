class AddApplicationFilledToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :application_filled, :string, :default => "n"
  end
end
