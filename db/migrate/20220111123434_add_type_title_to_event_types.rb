class AddTypeTitleToEventTypes < ActiveRecord::Migration[5.2]
  def change
    add_column :event_types, :type_title, :string
    EventType.create(type_title:"Online")
    EventType.create(type_title:"Local")

  end
end
