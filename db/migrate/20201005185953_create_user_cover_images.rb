class CreateUserCoverImages < ActiveRecord::Migration[5.2]
  def change
    create_table :user_cover_images do |t|
      t.references :user, foreign_key: true
      t.text :cover_path

      t.timestamps
    end
  end
end
