class CreateJobTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :job_types do |t|
      t.string :job_type_title

      t.timestamps
    end
    JobType.create(job_type_title:"Full Time")
    JobType.create(job_type_title:"Part Time")
  end
end
