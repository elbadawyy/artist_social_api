class CreateCommentReacts < ActiveRecord::Migration[5.2]
  def change
    create_table :comment_reacts do |t|
      t.references :user, foreign_key: true
      t.references :comment, foreign_key: true
      t.references :react, foreign_key: true

      t.timestamps
    end
  end
end
