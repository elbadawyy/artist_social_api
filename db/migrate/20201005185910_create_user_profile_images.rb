class CreateUserProfileImages < ActiveRecord::Migration[5.2]
  def change
    create_table :user_profile_images do |t|
      t.references :user, foreign_key: true
      t.text :image_path

      t.timestamps
    end
  end
end
