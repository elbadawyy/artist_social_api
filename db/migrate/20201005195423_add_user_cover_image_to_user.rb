class AddUserCoverImageToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :user_cover_image, foreign_key: true
  end
end
