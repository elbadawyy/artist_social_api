class CreateReacts < ActiveRecord::Migration[5.2]
  def change
    create_table :reacts do |t|
      t.string :react_title

      t.timestamps
    end
    React.create(react_title:"Like")
    React.create(react_title:"Love")
    React.create(react_title:"Celebrate")
  end
end
