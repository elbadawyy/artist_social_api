class CreateResetPasswordCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :reset_password_codes do |t|
      t.string :user_email
      t.string :code

      t.timestamps
    end
  end
end
