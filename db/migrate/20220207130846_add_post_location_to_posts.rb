class AddPostLocationToPosts < ActiveRecord::Migration[5.2]
  def change
    add_reference :posts, :post_location, foreign_key: true, :default => 1
  end
end
