class CreateApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :applications do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :phone_no
      t.string :another_phone_no
      t.string :email
      t.string :latitude
      t.string :longitude
      t.text :work_experience
      t.text :education_details
      t.text :portofolio_link
      t.text :cv_path

      t.timestamps
    end
  end
end
