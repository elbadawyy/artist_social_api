class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.string :chat_key
      t.references :user
      t.text :message_body

      t.timestamps
    end
  end
end
