class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :project_title
      t.references :user, foreign_key: true
      t.text :description
      t.text :cover_path
      t.string :project_link

      t.timestamps
    end
  end
end
