class AddMainFieldToFields < ActiveRecord::Migration[5.2]
  def change
    add_reference :fields, :main_field, foreign_key: true
  end
end
