class AddIsShareableAndIsCommentableToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :is_shareable, :string,:default => "y"
    add_column :posts, :is_commentable, :string,:default => "y"
  end
end
