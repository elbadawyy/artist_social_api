class ChangePostToUtf8mb4 < ActiveRecord::Migration[5.2]
  def up
    execute "ALTER TABLE posts CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"
    execute "ALTER TABLE posts MODIFY post_body TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"
  end
def down
    execute "ALTER TABLE posts CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin"
    execute "ALTER TABLE posts MODIFY post_body TEXT CHARACTER SET utf8 COLLATE utf8_bin"
  end
end
