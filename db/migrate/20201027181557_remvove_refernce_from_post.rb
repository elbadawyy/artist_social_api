class RemvoveRefernceFromPost < ActiveRecord::Migration[5.2]
  def change
    # remove_reference :posts, :shared_from_id, index: true, foreign_key: true

    remove_foreign_key :posts, column: :shared_from_id

    # change_column :posts, :shared_from_id, :string, :null => true
  end
end
