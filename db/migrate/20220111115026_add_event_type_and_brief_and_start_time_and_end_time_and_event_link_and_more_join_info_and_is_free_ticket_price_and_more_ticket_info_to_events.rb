class AddEventTypeAndBriefAndStartTimeAndEndTimeAndEventLinkAndMoreJoinInfoAndIsFreeTicketPriceAndMoreTicketInfoToEvents < ActiveRecord::Migration[5.2]
  def change
    add_reference :events, :event_type, foreign_key: true
    add_column :events, :brief, :text
    add_column :events, :start_time, :time
    add_column :events, :end_time, :time
    add_column :events, :event_link, :text
    add_column :events, :more_join_info, :text
    add_column :events, :is_free, :string
    add_column :events, :ticket_price, :string
    add_column :events, :more_ticket_info, :text
  end
end
