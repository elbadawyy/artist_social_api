class AddNotificationTypeToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :notification_type, :string,:default => "from_posts"
  end
end
