class AddUserProfileImageToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :user_profile_image, foreign_key: true
  end
end
