class AddBase64TypeToMessageCables < ActiveRecord::Migration[5.2]
  def change
    add_column :message_cables, :base64_type, :string
  end
end
