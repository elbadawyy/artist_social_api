class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.references :user, foreign_key: true
      t.text :post_body
      t.references :post_type, foreign_key: true

      t.timestamps
    end
  end
end
