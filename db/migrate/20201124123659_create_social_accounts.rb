class CreateSocialAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :social_accounts do |t|
      t.string :account_id
      t.string :from

      t.timestamps
    end
  end
end
