class CreatePostLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :post_locations do |t|
      t.string :location

      t.timestamps
    end
    PostLocation.create(location:"normal_timeline")
    PostLocation.create(location:"event_timeline")

  end
end
