class AddImageTypeToProjectImages < ActiveRecord::Migration[5.2]
  def change
    add_column :project_images, :image_type, :string, default: "image"

  end
end
