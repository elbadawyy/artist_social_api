class CreateMainFields < ActiveRecord::Migration[5.2]
  def change
    create_table :main_fields do |t|
      t.string :main_title

      t.timestamps
    end
  end
end
