class AddPhoneNoAndExtraPhoneNoAndEmailAddressAndWebsiteAndJobTypeAndLatitudeAndLongitudeToJobs < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :phone_no, :string
    add_column :jobs, :extra_phone_no, :string
    add_column :jobs, :email_address, :string
    add_column :jobs, :website, :text
    add_reference :jobs, :job_type, foreign_key: true
    add_column :jobs, :latitude, :string
    add_column :jobs, :longitude, :string
  end
end
