class AddFromAndToAndPlaceToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :from, :date
    add_column :events, :to, :date
    add_column :events, :place, :string
  end
end
