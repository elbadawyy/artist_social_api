class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.references :from, index: true, foreign_key: {to_table: :users}
      t.references :to, index: true, foreign_key: {to_table: :users}
      t.text :body

      t.timestamps
    end
  end
end
