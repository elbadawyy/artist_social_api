class AddCoverPathToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :cover_path, :text
  end
end
