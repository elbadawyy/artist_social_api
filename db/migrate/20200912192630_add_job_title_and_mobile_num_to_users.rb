class AddJobTitleAndMobileNumToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :job_title, :string
    add_column :users, :mobile_num, :string
  end
end
