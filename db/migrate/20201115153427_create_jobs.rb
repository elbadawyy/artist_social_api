class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.references :user, foreign_key: true
      t.string :job_title
      t.text :job_desc

      t.timestamps
    end
  end
end
