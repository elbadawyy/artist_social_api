class CreateUnreadedNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :unreaded_notifications do |t|
      t.references :user, foreign_key: true
      t.integer :unreaded_count

      t.timestamps
    end
  end
end
