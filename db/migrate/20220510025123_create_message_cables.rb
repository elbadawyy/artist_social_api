class CreateMessageCables < ActiveRecord::Migration[5.2]
  def change
    create_table :message_cables do |t|
      t.references :conversation, foreign_key: true
      t.text :content
      t.references :sender
      t.references :reciever


      t.timestamps
    end
  end
end
