class CreatePostTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :post_types do |t|
      t.string :post_type_desc

      t.timestamps
    end
    PostType.create(post_type_desc:"text")
    PostType.create(post_type_desc:"image")
    PostType.create(post_type_desc:"video")
    PostType.create(post_type_desc:"audio")
  end
end
