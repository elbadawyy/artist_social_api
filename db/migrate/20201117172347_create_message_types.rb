class CreateMessageTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :message_types do |t|
      t.string :message_type

      t.timestamps
    end
    MessageType.create(message_type:"text")
    MessageType.create(message_type:"image")
    MessageType.create(message_type:"audio")
    MessageType.create(message_type:"video")
    MessageType.create(message_type:"doc")
  end
end
