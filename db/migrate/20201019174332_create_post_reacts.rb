class CreatePostReacts < ActiveRecord::Migration[5.2]
  def change
    create_table :post_reacts do |t|
      t.references :post, foreign_key: true
      t.references :react, foreign_key: true

      t.timestamps
    end
  end
end
