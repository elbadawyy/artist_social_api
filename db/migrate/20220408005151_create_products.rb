class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :product_name
      t.string :product_details
      t.string :price
      t.references :country, foreign_key: true
      t.references :user, foreign_key: true
      t.string :currency
      t.string :phone
      t.string :email
      t.string :main_image_url

      t.timestamps
    end
  end
end
