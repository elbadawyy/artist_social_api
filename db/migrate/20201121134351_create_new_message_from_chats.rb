class CreateNewMessageFromChats < ActiveRecord::Migration[5.2]
  def change
    create_table :new_message_from_chats do |t|
      t.references :user, foreign_key: true
      t.integer :new_message_number
      t.string :chat_key

      t.timestamps
    end
  end
end
