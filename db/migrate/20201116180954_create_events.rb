class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :event_title
      t.references :user, foreign_key: true
      t.text :event_desc

      t.timestamps
    end
  end
end
