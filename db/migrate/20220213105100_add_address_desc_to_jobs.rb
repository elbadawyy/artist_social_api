class AddAddressDescToJobs < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :address_desc, :text
  end
end
