class CreateRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :roles do |t|
      t.string :role_title

      t.timestamps
    end

    Role.create(role_title:"Admin")
    Role.create(role_title:"Client")
  end
end
