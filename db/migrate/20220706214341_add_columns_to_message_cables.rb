class AddColumnsToMessageCables < ActiveRecord::Migration[5.2]
  def change
    add_column :message_cables, :base64_content, :text
    add_column :message_cables, :extension, :string
  end
end
