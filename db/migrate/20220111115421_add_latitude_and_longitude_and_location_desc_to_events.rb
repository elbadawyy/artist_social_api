class AddLatitudeAndLongitudeAndLocationDescToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :latitude, :string
    add_column :events, :longitude, :string
    add_column :events, :location_desc, :text
  end
end
