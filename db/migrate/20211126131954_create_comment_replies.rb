class CreateCommentReplies < ActiveRecord::Migration[5.2]
  def change
    create_table :comment_replies do |t|
      t.references :comment, foreign_key: true
      t.references :user, foreign_key: true
      t.text :reply_body

      t.timestamps
    end
  end
end
