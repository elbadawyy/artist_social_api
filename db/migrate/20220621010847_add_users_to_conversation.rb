class AddUsersToConversation < ActiveRecord::Migration[5.2]
  def change
    add_column :conversations, :users, :string
  end
end
