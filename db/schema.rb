# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_07_14_193230) do

  create_table "applications", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.string "phone_no"
    t.string "another_phone_no"
    t.string "email"
    t.string "latitude"
    t.string "longitude"
    t.text "work_experience"
    t.text "education_details"
    t.text "portofolio_link"
    t.text "cv_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "adress"
    t.index ["user_id"], name: "index_applications_on_user_id"
  end

  create_table "blocked_users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "blocked_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blocked_user_id"], name: "index_blocked_users_on_blocked_user_id"
    t.index ["user_id"], name: "index_blocked_users_on_user_id"
  end

  create_table "comment_reacts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "comment_id"
    t.bigint "react_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["comment_id"], name: "index_comment_reacts_on_comment_id"
    t.index ["react_id"], name: "index_comment_reacts_on_react_id"
    t.index ["user_id"], name: "index_comment_reacts_on_user_id"
  end

  create_table "comment_replies", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "comment_id"
    t.bigint "user_id"
    t.text "reply_body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["comment_id"], name: "index_comment_replies_on_comment_id"
    t.index ["user_id"], name: "index_comment_replies_on_user_id"
  end

  create_table "comments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "post_id"
    t.text "comment_body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["post_id"], name: "index_comments_on_post_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "conversations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "room_code"
    t.string "users"
  end

  create_table "countries", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "country_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "event_going_users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "event_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_event_going_users_on_event_id"
    t.index ["user_id"], name: "index_event_going_users_on_user_id"
  end

  create_table "event_intersted_users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "event_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_event_intersted_users_on_event_id"
    t.index ["user_id"], name: "index_event_intersted_users_on_user_id"
  end

  create_table "event_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type_title"
  end

  create_table "events", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "event_title"
    t.bigint "user_id"
    t.text "event_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "from"
    t.date "to"
    t.string "place"
    t.bigint "event_type_id"
    t.text "brief"
    t.time "start_time"
    t.time "end_time"
    t.text "event_link"
    t.text "more_join_info"
    t.string "is_free"
    t.string "ticket_price"
    t.text "more_ticket_info"
    t.string "latitude"
    t.string "longitude"
    t.text "location_desc"
    t.text "cover_path"
    t.index ["event_type_id"], name: "index_events_on_event_type_id"
    t.index ["user_id"], name: "index_events_on_user_id"
  end

  create_table "fields", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "field_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "main_field_id"
    t.index ["main_field_id"], name: "index_fields_on_main_field_id"
  end

  create_table "followers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "follower_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["follower_id"], name: "index_followers_on_follower_id"
    t.index ["user_id"], name: "index_followers_on_user_id"
  end

  create_table "hashtags", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.text "hashtag_body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "job_applications", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "job_id"
    t.bigint "application_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["application_id"], name: "index_job_applications_on_application_id"
    t.index ["job_id"], name: "index_job_applications_on_job_id"
  end

  create_table "job_intersted_users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "job_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["job_id"], name: "index_job_intersted_users_on_job_id"
    t.index ["user_id"], name: "index_job_intersted_users_on_user_id"
  end

  create_table "job_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "job_type_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "jobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.string "job_title"
    t.text "job_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "duties"
    t.string "phone_no"
    t.string "extra_phone_no"
    t.string "email_address"
    t.text "website"
    t.bigint "job_type_id"
    t.string "latitude"
    t.string "longitude"
    t.text "address_desc"
    t.index ["job_type_id"], name: "index_jobs_on_job_type_id"
    t.index ["user_id"], name: "index_jobs_on_user_id"
  end

  create_table "main_fields", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "main_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "message_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "message_id"
    t.text "attachment_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["message_id"], name: "index_message_attachments_on_message_id"
  end

  create_table "message_cables", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "conversation_id"
    t.text "content"
    t.bigint "sender_id"
    t.bigint "reciever_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "base64_content"
    t.string "extension"
    t.string "base64_type"
    t.index ["conversation_id"], name: "index_message_cables_on_conversation_id"
    t.index ["reciever_id"], name: "index_message_cables_on_reciever_id"
    t.index ["sender_id"], name: "index_message_cables_on_sender_id"
  end

  create_table "message_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "message_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.text "message_body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "message_type_id", default: 1
    t.index ["message_type_id"], name: "index_messages_on_message_type_id"
  end

  create_table "new_message_from_chats", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "new_message_number"
    t.string "chat_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_new_message_from_chats_on_user_id"
  end

  create_table "notifications", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "from_id"
    t.bigint "to_id"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "notification_type", default: "from_posts"
    t.integer "target_id"
    t.index ["from_id"], name: "index_notifications_on_from_id"
    t.index ["to_id"], name: "index_notifications_on_to_id"
  end

  create_table "post_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "post_id"
    t.text "attachment_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_post_attachments_on_post_id"
  end

  create_table "post_hashtags", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "post_id"
    t.bigint "hashtag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hashtag_id"], name: "index_post_hashtags_on_hashtag_id"
    t.index ["post_id"], name: "index_post_hashtags_on_post_id"
  end

  create_table "post_locations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "post_reacts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "post_id"
    t.bigint "react_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["post_id"], name: "index_post_reacts_on_post_id"
    t.index ["react_id"], name: "index_post_reacts_on_react_id"
    t.index ["user_id"], name: "index_post_reacts_on_user_id"
  end

  create_table "post_tags", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "post_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_post_tags_on_post_id"
    t.index ["user_id"], name: "index_post_tags_on_user_id"
  end

  create_table "post_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "post_type_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "user_id"
    t.text "post_body"
    t.bigint "post_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "shared_from_id"
    t.string "is_shareable", default: "y"
    t.string "is_commentable", default: "y"
    t.text "post_body_html"
    t.bigint "post_location_id", default: 1
    t.bigint "event_id"
    t.index ["event_id"], name: "index_posts_on_event_id"
    t.index ["post_location_id"], name: "index_posts_on_post_location_id"
    t.index ["post_type_id"], name: "index_posts_on_post_type_id"
    t.index ["shared_from_id"], name: "index_posts_on_shared_from_id"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "product_images", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "product_id"
    t.string "image_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_images_on_product_id"
  end

  create_table "products", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "product_name"
    t.string "product_details"
    t.string "price"
    t.bigint "country_id"
    t.bigint "user_id"
    t.string "currency"
    t.string "phone"
    t.string "email"
    t.string "main_image_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "location"
    t.index ["country_id"], name: "index_products_on_country_id"
    t.index ["user_id"], name: "index_products_on_user_id"
  end

  create_table "project_fields", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "project_id"
    t.bigint "field_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["field_id"], name: "index_project_fields_on_field_id"
    t.index ["project_id"], name: "index_project_fields_on_project_id"
  end

  create_table "project_images", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "project_id"
    t.text "image_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_type", default: "image"
    t.index ["project_id"], name: "index_project_images_on_project_id"
  end

  create_table "project_tags", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "project_id"
    t.string "tag_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_project_tags_on_project_id"
  end

  create_table "projects", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "project_title"
    t.bigint "user_id"
    t.text "description"
    t.text "cover_path"
    t.string "project_link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "reacts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "react_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reset_password_codes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "user_email"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "role_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "social_accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "account_id"
    t.string "from"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.bigint "user_id"
    t.index ["user_id"], name: "index_social_accounts_on_user_id"
  end

  create_table "unreaded_notifications", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "unreaded_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_unreaded_notifications_on_user_id"
  end

  create_table "user_conversations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "conversation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["conversation_id"], name: "index_user_conversations_on_conversation_id"
    t.index ["user_id"], name: "index_user_conversations_on_user_id"
  end

  create_table "user_cover_images", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.text "cover_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_cover_images_on_user_id"
  end

  create_table "user_fcm_tokens", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.string "fcm_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_fcm_tokens_on_user_id"
  end

  create_table "user_fields", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "field_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["field_id"], name: "index_user_fields_on_field_id"
    t.index ["user_id"], name: "index_user_fields_on_user_id"
  end

  create_table "user_messages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "message_id"
    t.string "chat_key"
    t.integer "sender_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "reciever_id"
    t.index ["message_id"], name: "index_user_messages_on_message_id"
    t.index ["user_id"], name: "index_user_messages_on_user_id"
  end

  create_table "user_profile_images", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.text "image_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_profile_images_on_user_id"
  end

  create_table "user_skills", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.bigint "user_id"
    t.string "skill_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_skills_on_user_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.date "date_of_birth"
    t.string "gender"
    t.bigint "country_id"
    t.string "job_title"
    t.string "mobile_num"
    t.string "profile_img"
    t.text "about_me"
    t.bigint "user_profile_image_id"
    t.bigint "user_cover_image_id"
    t.string "add_call_allowed", default: "n"
    t.bigint "role_id", default: 2
    t.string "is_blocked"
    t.string "application_filled", default: "n"
    t.index ["country_id"], name: "index_users_on_country_id"
    t.index ["role_id"], name: "index_users_on_role_id"
    t.index ["user_cover_image_id"], name: "index_users_on_user_cover_image_id"
    t.index ["user_profile_image_id"], name: "index_users_on_user_profile_image_id"
  end

  add_foreign_key "applications", "users"
  add_foreign_key "blocked_users", "users"
  add_foreign_key "blocked_users", "users", column: "blocked_user_id"
  add_foreign_key "comment_reacts", "comments"
  add_foreign_key "comment_reacts", "reacts"
  add_foreign_key "comment_reacts", "users"
  add_foreign_key "comment_replies", "comments"
  add_foreign_key "comment_replies", "users"
  add_foreign_key "comments", "users"
  add_foreign_key "event_going_users", "events"
  add_foreign_key "event_going_users", "users"
  add_foreign_key "event_intersted_users", "events"
  add_foreign_key "event_intersted_users", "users"
  add_foreign_key "events", "event_types"
  add_foreign_key "events", "users"
  add_foreign_key "fields", "main_fields"
  add_foreign_key "followers", "users"
  add_foreign_key "followers", "users", column: "follower_id"
  add_foreign_key "job_applications", "applications"
  add_foreign_key "job_applications", "jobs"
  add_foreign_key "job_intersted_users", "jobs"
  add_foreign_key "job_intersted_users", "users"
  add_foreign_key "jobs", "job_types"
  add_foreign_key "jobs", "users"
  add_foreign_key "message_attachments", "messages"
  add_foreign_key "message_cables", "conversations"
  add_foreign_key "messages", "message_types"
  add_foreign_key "new_message_from_chats", "users"
  add_foreign_key "notifications", "users", column: "from_id"
  add_foreign_key "notifications", "users", column: "to_id"
  add_foreign_key "post_attachments", "posts"
  add_foreign_key "post_hashtags", "hashtags"
  add_foreign_key "post_hashtags", "posts"
  add_foreign_key "post_reacts", "posts"
  add_foreign_key "post_reacts", "reacts"
  add_foreign_key "post_reacts", "users"
  add_foreign_key "post_tags", "posts"
  add_foreign_key "post_tags", "users"
  add_foreign_key "posts", "events"
  add_foreign_key "posts", "post_locations"
  add_foreign_key "posts", "post_types"
  add_foreign_key "posts", "users"
  add_foreign_key "product_images", "products"
  add_foreign_key "products", "countries"
  add_foreign_key "products", "users"
  add_foreign_key "project_fields", "fields"
  add_foreign_key "project_fields", "projects"
  add_foreign_key "project_images", "projects"
  add_foreign_key "project_tags", "projects"
  add_foreign_key "projects", "users"
  add_foreign_key "social_accounts", "users"
  add_foreign_key "unreaded_notifications", "users"
  add_foreign_key "user_conversations", "conversations"
  add_foreign_key "user_conversations", "users"
  add_foreign_key "user_cover_images", "users"
  add_foreign_key "user_fcm_tokens", "users"
  add_foreign_key "user_fields", "fields"
  add_foreign_key "user_fields", "users"
  add_foreign_key "user_messages", "messages"
  add_foreign_key "user_messages", "users"
  add_foreign_key "user_profile_images", "users"
  add_foreign_key "user_skills", "users"
  add_foreign_key "users", "countries"
  add_foreign_key "users", "roles"
  add_foreign_key "users", "user_cover_images"
  add_foreign_key "users", "user_profile_images"
end
