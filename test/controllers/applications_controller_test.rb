require 'test_helper'

class ApplicationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @application = applications(:one)
  end

  test "should get index" do
    get applications_url, as: :json
    assert_response :success
  end

  test "should create application" do
    assert_difference('Application.count') do
      post applications_url, params: { application: { another_phone_no: @application.another_phone_no, cv_path: @application.cv_path, education_details: @application.education_details, email: @application.email, latitude: @application.latitude, longitude: @application.longitude, name: @application.name, phone_no: @application.phone_no, portofolio_link: @application.portofolio_link, user_id: @application.user_id, work_experience: @application.work_experience } }, as: :json
    end

    assert_response 201
  end

  test "should show application" do
    get application_url(@application), as: :json
    assert_response :success
  end

  test "should update application" do
    patch application_url(@application), params: { application: { another_phone_no: @application.another_phone_no, cv_path: @application.cv_path, education_details: @application.education_details, email: @application.email, latitude: @application.latitude, longitude: @application.longitude, name: @application.name, phone_no: @application.phone_no, portofolio_link: @application.portofolio_link, user_id: @application.user_id, work_experience: @application.work_experience } }, as: :json
    assert_response 200
  end

  test "should destroy application" do
    assert_difference('Application.count', -1) do
      delete application_url(@application), as: :json
    end

    assert_response 204
  end
end
