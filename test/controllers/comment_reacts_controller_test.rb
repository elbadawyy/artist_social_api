require 'test_helper'

class CommentReactsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comment_react = comment_reacts(:one)
  end

  test "should get index" do
    get comment_reacts_url, as: :json
    assert_response :success
  end

  test "should create comment_react" do
    assert_difference('CommentReact.count') do
      post comment_reacts_url, params: { comment_react: { comment_id: @comment_react.comment_id, react_id: @comment_react.react_id, user_id: @comment_react.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show comment_react" do
    get comment_react_url(@comment_react), as: :json
    assert_response :success
  end

  test "should update comment_react" do
    patch comment_react_url(@comment_react), params: { comment_react: { comment_id: @comment_react.comment_id, react_id: @comment_react.react_id, user_id: @comment_react.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy comment_react" do
    assert_difference('CommentReact.count', -1) do
      delete comment_react_url(@comment_react), as: :json
    end

    assert_response 204
  end
end
