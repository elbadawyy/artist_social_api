class RoomChannel < ApplicationCable::Channel
    # calls when a client connects to the server
    def subscribed
        # creates a private chat room with a unique name
        stream_from("ChatRoom-#{(params[:room_id])}") if params[:room_id]
        stream_from("received-#{(params[:user_id])}")
    end

    
    # calls when a client broadcasts data
    def speak(data)
      sender = User.find(data['sender_id'])
      reciever = User.find(data['reciever_id'])
      room_id = data['room_id']
      message = data['message']
      base64 = data['base64_content']
      extension = data['extension']
      base64_type = data['base64_type']
  
      raise 'No room_id!' if room_id.blank?
      convo = Conversation.find_or_create_by(room_code: room_id)
      raise 'No conversation found!' if convo.blank?
  
      # adds the message sender to the conversation if not already included
      #convo.users << sender unless convo.users.include?(sender)
      # saves the message and its data to the DB
      # Note: this does not broadcast to the clients yet!
      MessageCable.create!(
        conversation: convo,
        sender: sender,
        content: message ,
        reciever: reciever ,
        base64_content: base64 ,
        extension: extension ,
        base64_type: base64_type

      )
    end
  
  
end
  