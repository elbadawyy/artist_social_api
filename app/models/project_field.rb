class ProjectField < ApplicationRecord
  belongs_to :project
  belongs_to :field
end
