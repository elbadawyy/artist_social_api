class MessageCable < ApplicationRecord
    belongs_to :conversation
    belongs_to :sender, :class_name => 'User'
    belongs_to :reciever, :class_name => 'User'
    

    after_create_commit -> { broadcast_message(self) }

    def broadcast_message(message)
        payload = {
            room_id: message.conversation.id,
            content: message.content,
            sender_id: message.sender_id,
            reciever_id: message.reciever_id ,
            base64_content: message.base64_content ,
            extension: message.extension , 
            base64_type: message.base64_type
        }

        room_id = {room_id: message.conversation.id}
        ActionCable.server.broadcast(build_room_id(message.conversation.id), payload)
        ActionCable.server.broadcast("received-#{message.reciever_id}", room_id)

    end

    
    def build_room_id(id)
        "ChatRoom-#{id}"
    end
    
    

end
