class Hashtag < ApplicationRecord
  has_many :post_hashtags, :dependent => :delete_all
end
