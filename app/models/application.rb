class Application < ApplicationRecord
  belongs_to :user
  has_many :job_applications, dependent: :destroy

  attr_accessor :is_marked

  def user_info
    self.user.user_info
  end

end
