class UserMessage < ApplicationRecord
  include ActionView::Helpers::DateHelper

  belongs_to :user
  belongs_to :message

  @@current_user = {}

  def self.inject_curent_user(user)
    @@current_user=user
  end

  def while_ago
      distance_of_time_in_words(self.created_at, Time.now, {})

  end

  def owner_info
      user=User.find(self.sender_id)
        userInfo={}
        userInfo[:id] = user.id
        userInfo[:full_name] = "#{ user.first_name } #{ user.last_name }"
        userInfo[:get_profile_path]=user.get_profile_path
        userInfo[:gender]=user.gender
        userInfo[:job_title]=user.job_title

      return userInfo  
  end

  def attachment_path
      if (self.message.message_type_id == 1 ||  self.message.message_attachments.last.nil?)
          return ""
      else
          return self.message.message_attachments.last.attachment_path
      end
  end

  def unreaded_messages
      unreaded=NewMessageFromChat.find_by(user_id:@@current_user.id,chat_key:self.chat_key)
      if unreaded
          return unreaded.new_message_number
      else
          return 0
      end
  end

end
