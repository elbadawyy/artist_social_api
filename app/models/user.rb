class User < ApplicationRecord
    validates_uniqueness_of :email
    # validates_uniqueness_of :mobile_num
    has_secure_password

    belongs_to :country, optional: true
    has_many :user_fields , dependent: :destroy
    has_many :user_skills, dependent: :destroy
    has_many :social_accounts, dependent: :destroy
    has_many :followers, dependent: :destroy
    has_many :blocked_users, dependent: :destroy
    has_many :posts, dependent: :destroy
    has_many :projects, dependent: :destroy
    has_many :new_message_from_chats, dependent: :destroy
    has_many :comments, dependent: :destroy
    has_many :jobs, dependent: :destroy
    has_many :events, dependent: :destroy
    has_many :job_intersted_users, dependent: :destroy
    has_many :event_intersted_users, dependent: :destroy
    has_many :event_going_users, dependent: :destroy
    has_many :post_reacts, dependent: :destroy
    has_one :user_profile_image, dependent: :destroy
    has_one :user_cover_image, dependent: :destroy
    has_one :application, dependent: :destroy
    has_many :products
    has_many :user_conversations
    has_many :conversations , through: :user_conversations
    after_create :send_user_info_to_mailchimp
    has_many :sent_messages , class_name: 'MessageCable', foreign_key: 'sender_id' , dependent: :destroy
    has_many :received_messages , class_name: 'MessageCable', foreign_key: 'reciever_id' , dependent: :destroy


    
    attr_accessor :is_following


    def user_fields_names
        self.user_fields.joins(:field).select("fields.*")
    end

    def followers_number
        Follower.where(follower_id:self.id).count
    end

    def following_number
        Follower.where(user_id:self.id).count

    end

    def get_profile_path
        if ! self.user_profile_image_id
            return ""
        end

        user_image_path=UserProfileImage.find(self.user_profile_image_id)
        if user_image_path
            return user_image_path.image_path

        else
            return ""
        end
        # self.user_profile_image.image_path
        # return self.user_profile_image.present? ?  self.user_profile_image.image_path: ""

    end

    def get_cover_path
        if ! self.user_cover_image_id
            return ""
        end

        user_image_path=UserCoverImage.find(self.user_cover_image_id)
        if user_image_path
            return user_image_path.cover_path

        else
            return ""
        end

        # return self.user_cover_image.present? ?  self.user_cover_image.cover_path: ""
    end

    def skills
        return self.user_skills
    end

  

    def send_user_info_to_mailchimp
       gibbon = Gibbon::Request.new(api_key: "676220ec043f60d9bdc397a47005e934-us1")
       gibbon.timeout = 10
       gibbon.lists("9bb03240e7").members.create(body: {email_address: self.email , status: "subscribed", merge_fields: {FNAME: self.first_name, LNAME: self.last_name}})

    end
    
    def user_info
        info={}
        info[:id]=self.id
        info[:full_name]="#{self.name}"
        info[:get_profile_path]=self.get_profile_path
        info[:job_title]=self.job_title
        info[:gender]=self.gender
        return info
    end

end


