class Post < ApplicationRecord
  include ActionView::Helpers::DateHelper

  belongs_to :user
  belongs_to :post_type, required: false
  has_many :post_attachments, :dependent => :delete_all
  has_many :post_reacts, :dependent => :delete_all
  has_many :comments, :dependent => :delete_all
  has_many :post_hashtags, :dependent => :delete_all
  has_many :post_tags, :dependent => :delete_all
  

  @@current_user = {}
  @@date_range = {}

  def while_ago
    distance_of_time_in_words(self.created_at, Time.now, {})

  end

  def items_no
    Post.where(posts:{created_at: @@date_range,shared_from_id: nil}).count
  end

  def user_info
    user={}
    if ! self.user.present? 
      return user  
    end

    user[:id] = self.user.id
    user[:full_name] = "#{ self.user.name }"
    user[:get_profile_path]=self.user.get_profile_path
    user[:gender]=self.user.gender
    return user  
    # authr=User.find(self.user.id)
    # authr.joins(:user_profile_image).select("users.frist_name,users.last_name,user_profile_images.image_path")
  end

  def attachment_path
    path = self.post_attachments.count == 0 ? "" : self.post_attachments.last.attachment_path 
    return path
  end

  def original_post
    return {} if ! self.shared_from_id.present?
    @@current_user
      
    return {is_deleted:"true"} if Post.where(id:self.shared_from_id).count == 0 || BlockedUser.where(user_id:@@current_user.id ,blocked_user_id:Post.find(self.shared_from_id).user.id ).count > 0 || BlockedUser.where(user_id:Post.find(self.shared_from_id).user.id  ,blocked_user_id: @@current_user.id).count > 0
    post=Post.find(self.shared_from_id)
    user=User.find(post.user_id)
    original_attachment_path = post.post_attachments.count == 0 ? "" : post.post_attachments.last.attachment_path
    # user[:get_profile_path]=self.user.get_profile_path

    return {
    post:post,author_info:user,
    author_profile_path:user.get_profile_path,original_attachment_path:original_attachment_path,
    original_post_while_ago: distance_of_time_in_words(post.created_at, Time.now, {}),
    is_deleted:"false"
    } 
  end

  def reacts
    likes_count=PostReact.where(post_id: self.id,react_id:1).count
    love_count=PostReact.where(post_id: self.id,react_id:2).count
    celeb_count=PostReact.where(post_id: self.id,react_id:3).count

    return {likes_count:likes_count,love_count:love_count, celeb_count:celeb_count}
  end

  def shares_no
    return Post.where(shared_from_id: self.id).count
  end

  def current_react
    react=PostReact.find_by(post_id:self.id,user_id:@@current_user.id)
    if react
      return react.react.id
    else
      return 0

    end
    # return @@current_user.id
  end

  def comments_no
    return self.comments.count
  end

  def self.inject_curent_user(user)
    @@current_user=user
  end


  def last_comment
    comment=Comment.where(post_id:self.id).order("created_at").last
    if comment
      last_commenter_info=comment.commenter_info
      while_ago=comment.while_ago
      comment_react=comment.current_react
      return {comment:comment,commenter:last_commenter_info,while_ago:while_ago,reacts:comment.reacts,replies:comment.replies,comment_react:comment_react}
    else
      return {}
    end
    
  end

  def reacts
    likes_count=PostReact.where(post_id: self.id,react_id:1).count
    love_count=PostReact.where(post_id: self.id,react_id:2).count
    celeb_count=PostReact.where(post_id: self.id,react_id:3).count

    return {likes_count:likes_count,love_count:love_count, celeb_count:celeb_count}
  end

  def post_reacts_details
    all=JSON.parse( PostReact.joins(post: {user: :user_profile_image}).where(post_id: self.id)
    .select("users.id,users.name,user_profile_images.image_path,post_reacts.react_id,post_reacts.user_id").uniq
    .to_json(:methods => :is_following) )


    likes=JSON.parse( PostReact.joins(post: {user: :user_profile_image}).where(post_id: self.id,react_id:1)
      .select("users.id,users.name,user_profile_images.image_path,post_reacts.react_id,post_reacts.user_id").uniq.to_json(:methods => :is_following) )

    love=JSON.parse( PostReact.joins(post: {user: :user_profile_image}).where(post_id: self.id,react_id:2)
    .select("users.id,users.name,user_profile_images.image_path,post_reacts.react_id,post_reacts.user_id").uniq.to_json(:methods => :is_following) )

    celeb=JSON.parse( PostReact.joins(post: {user: :user_profile_image}).where(post_id: self.id,react_id:3)
      .select("users.id,users.name,user_profile_images.image_path,post_reacts.react_id,post_reacts.user_id").uniq.to_json(:methods => :is_following) )

    return {all:all,likes:likes,love:love, celeb:celeb}


  end

  def included_hashtags
    Post.joins(post_hashtags: :hashtag).where(posts:{id:self.id}).select("hashtags.hashtag_body").map(&:hashtag_body)
  end

  def included_tags_user_info
      user_info_array=[]
      self.post_tags.map{|post_tag|
        user=User.find(post_tag.user_id)
        user_info_array << user.user_info

      }

      user_info_array
   



  end

end
