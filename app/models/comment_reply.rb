class CommentReply < ApplicationRecord
  include ActionView::Helpers::DateHelper

  belongs_to :comment
  belongs_to :user

  def while_ago
      distance_of_time_in_words(self.created_at, Time.now, {})
  end

  def replier_info
    self.user.user_info
  end
end
