class Notification < ApplicationRecord
    include ActionView::Helpers::DateHelper

    def while_ago
        return distance_of_time_in_words(self.created_at, Time.now, {})
    end

    def from_user_info
        u=User.find(self.from_id)
        if ! u.present?
            return {}
        end
        user={}
        user[:id] = u.id
        user[:full_name] = "#{ u.name }"
        user[:get_profile_path]=u.get_profile_path
        user[:gender]=u.gender
        user[:country_name]=u.country.present? ? u.country.country_name : ""
        user[:job_title]=u.job_title
        return user  
    

    end


    def to_user_info
        u=User.find(self.from_id)
        user={}
        user[:id] = u.id
        user[:full_name] = "#{ u.name }"
        user[:get_profile_path]=u.get_profile_path
        user[:gender]=u.gender
        user[:country_name]=u.country.present? ? u.country.country_name : ""
        user[:job_title]=u.job_title
        return user  
    

    end
end
