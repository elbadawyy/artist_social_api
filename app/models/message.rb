class Message < ApplicationRecord

    # belongs_to :user
    # belongs_to :message_type

    has_many :message_attachments, :dependent => :delete_all
    has_many :user_messages
    
end
