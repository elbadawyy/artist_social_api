class Event < ApplicationRecord
  belongs_to :user

  has_many :event_going_users, :dependent => :delete_all
  has_many :event_intersted_users, :dependent => :delete_all

  def intersted_going_users
    return {
      intersted: self.event_intersted_users.count,
      going: self.event_going_users.count
    }
  end

  @@current_user = {}

  def self.inject_curent_user(user)
    @@current_user=user
  end

  def current_state
    return {
      is_going: EventGoingUser.where(user_id:@@current_user.id,event_id:self.id).count > 0 ? "true" : "false",
      is_intersted: EventInterstedUser.where(user_id:@@current_user.id,event_id:self.id).count > 0 ? "true" : "false"
    }
  end
  
  def user_info
    user={}
    user[:id] = self.user.id
    user[:full_name] = "#{ self.user.first_name } #{ self.user.last_name }"
    user[:get_profile_path]=self.user.get_profile_path
    user[:gender]=self.user.gender
    user[:country_name]=self.user.country.present? ? self.user.country.country_name : ""
    user[:job_title]=self.user.job_title
    return user  
  end


end
