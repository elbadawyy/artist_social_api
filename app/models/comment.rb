class Comment < ApplicationRecord
    include ActionView::Helpers::DateHelper

    belongs_to :post
    belongs_to :user

    has_many :comment_replies

     @@current_user = {}

      def self.inject_curent_user(user)
        @@current_user=user
      end

      def current_react
        react=CommentReact.find_by(comment_id:self.id,user_id:@@current_user.id)
        if react
          return react.react_id
        else
          return 0
        end
      end

    def commenter_info
        info={}
        info[:id]=self.user.id
        info[:full_name]="#{self.user.name}"
        info[:get_profile_path]=self.user.get_profile_path
        info[:job_title]=self.user.job_title
        info[:gender]=self.user.gender
        return info
    end

    def while_ago
        distance_of_time_in_words(self.created_at, Time.now, {})
    end

    def reacts
        likes_count=CommentReact.where(comment_id: self.id,react_id:1).count
        love_count=CommentReact.where(comment_id: self.id,react_id:2).count
        celeb_count=CommentReact.where(comment_id: self.id,react_id:3).count

        return {likes_count:likes_count,love_count:love_count, celeb_count:celeb_count}
    end

    def replies
        replies=[]
        replies=self.comment_replies.map { |reply|  
            {
                reply:reply,
                replier_info:reply.replier_info,
                while_ago:reply.while_ago
            }

        }
    end
end
