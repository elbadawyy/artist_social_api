class Project < ApplicationRecord
  include ActionView::Helpers::DateHelper

  belongs_to :user
  has_many :project_images, dependent: :destroy
  has_many :project_tags, dependent: :destroy
  has_many :project_fields, dependent: :destroy

  attr_accessor :is_following


  def while_ago
    distance_of_time_in_words(self.created_at, Time.now, {})

  end

  def main_field
    self.project_fields.present? ? self.project_fields[0].field.main_field : {}
  end

  def user_info
    user={}
    if ! self.user.present?
      return user
    end
    user[:id] = self.user.id
    user[:full_name] = "#{ self.user.name }"
    user[:get_profile_path]=self.user.get_profile_path
    user[:gender]=self.user.gender
    user[:country_name]=self.user.country.present? ? self.user.country.country_name : ""
    user[:job_title]=self.user.job_title
    return user  
    # authr=User.find(self.user.id)
    # authr.joins(:user_profile_image).select("users.frist_name,users.last_name,user_profile_images.image_path")
  end

  def project_field_names
    return self.project_fields.joins(:field).select("fields.id,fields.field_name")
  end

  
end
