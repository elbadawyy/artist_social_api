class PostReact < ApplicationRecord
  belongs_to :post
  belongs_to :react
  belongs_to :user

  @@current_user = {}

  def user_info
    self.user.user_info
  end


  def self.inject_curent_user(user)
    @@current_user=user
  end

  def is_following
    Follower.find_by(user_id: @@current_user.id,
    follower_id:self.user_id).present? ? true : false

    # self
  end

end
