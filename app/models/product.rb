class Product < ApplicationRecord
  #validation 
  validates :product_name, :price, :currency , :location , presence: true
  #relations
  belongs_to :country
  belongs_to :user
  has_many :product_images , dependent: :destroy

  def self.convert_images(base64_image , extension)
    attachment_path = "products/images/#{String(Time.now.to_i)}_#{Random.rand(0...10000)}.#{extension}"
      dir = File.dirname("public/#{attachment_path}")
      FileUtils.mkdir_p(dir) unless File.directory?(dir)
      
      File.open("public/#{attachment_path}", 'wb') do |f|
        f.write(Base64.decode64(base64_image))
      end
      attachment_path
  end
end
