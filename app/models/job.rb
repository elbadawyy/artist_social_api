class Job < ApplicationRecord
  include ActionView::Helpers::DateHelper

  belongs_to :user
  has_many :job_intersted_users, :dependent => :delete_all
  has_many :job_applications, dependent: :destroy
  has_many :job_fields, dependent: :destroy



  @@current_user = {}


  def self.inject_curent_user(user)
    @@current_user=user
  end

  def intersted_no
    self.job_intersted_users.count
  end

  def while_ago
    distance_of_time_in_words(self.created_at, Time.now, {})

  end

  def current_state
    if (JobInterstedUser.where(user_id:@@current_user.id,job_id:self.id).count == 0 )
      return "false"
    else
      return "true"
    end

  end

  def is_applied
    if (@@current_user.application && JobApplication.find_by(application_id:@@current_user.application.id,job_id:self.id).present?)
      return "y"
    else
      return "n"
    end
  end

   def is_saved
    if (SavedJob.find_by(user_id:@@current_user.id,job_id:self.id).present?)
      return "y"
    else
      return "n"
    end
  end


  def user_info
    user={}
    user[:id] = self.user.id
    user[:full_name] = "#{ self.user.first_name } #{ self.user.last_name }"
    user[:get_profile_path]=self.user.get_profile_path
    user[:gender]=self.user.gender
    user[:country_name]=self.user.country.present? ? self.user.country.country_name : ""
    user[:job_title]=self.user.job_title
    return user  
  end

  def job_fields_names
    self.job_fields.joins(:field).select("job_fields.field_id,fields.field_name")
  end

end
