class AuthenticateUser
  prepend SimpleCommand

  def initialize(email, password)
    @email = email
    @password = password
  end

  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  def self.social_user_request(account_id)
    user = SocialAccount.find_by_account_id(account_id)
    
    if user 
      token=JsonWebToken.encode(user_id: user.user.id)

      return token

    end

    # errors.add :user_authentication, 'invalid credentials'
    nil


  end

  private

  attr_accessor :email, :password
  def social_user_private(email)
    
  end


  def user
    user = User.find_by_email(email)
    if user && user.authenticate(password)
      
      return user
    else
      user = User.find_by_mobile_num(email)
      if user && user.authenticate(password)
        
        return user
      end

    end

    errors.add :user_authentication, 'invalid credentials'
    nil
  end
end
