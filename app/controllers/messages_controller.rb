class MessagesController < ApplicationController
  include MessageHelper
  include NotificationHelper

  before_action :set_message, only: [:show, :update]

  # GET /messages
  def index
    @messages = Message.all

    render json: @messages
  end

  # GET /messages/1
  def show
    render json: @message
  end
  
  def get_total_unreaded_message
    render json: {count: NewMessageFromChat.where(user_id:@current_user.id).sum(:new_message_number)}
  end

  def list_messages_with_users
    messages_chat_keys=UserMessage.where(user_id: @current_user.id).distinct.pluck(:chat_key)
    messages=Array.new
    messages_chat_keys.map{ |chat_key|
      msg_obj={}
      user_message=UserMessage.where(chat_key:chat_key).last
      message=Message.find(user_message.message_id)
      msg_obj[:message_body]=message.message_body
      msg_obj[:while_ago]=user_message.while_ago
      msg_obj[:message_type_id]=message.message_type_id
      UserMessage.inject_curent_user(@current_user)

      msg_obj[:unreaded_messages]=user_message.unreaded_messages

      if (user_message.sender_id == @current_user.id)
        msg_obj[:user_obj] = User.find(user_message.reciever_id)
        msg_obj[:from_me] == "y"
        msg_obj[:profile_image] = msg_obj[:user_obj].get_profile_path

      else
        msg_obj[:user_obj] = User.find(user_message.sender_id)
        msg_obj[:from_me] == "n"

        msg_obj[:profile_image] = msg_obj[:user_obj].get_profile_path

      end
        messages << msg_obj
    }
    render json: messages
  end

  def list_message_from_user
    chat_key=generate_chat_key(@current_user.id,params[:with_user_id])
    make_chat_read(@current_user.id,chat_key)
    render json:UserMessage.joins(:message).where(user_messages:{user_id:@current_user.id,chat_key:chat_key})
      .select("user_messages.*,messages.message_body,messages.message_type_id"),methods: [:owner_info,:while_ago,:attachment_path]
  end

  # POST /messages
  def create
    message = Message.create(message_body: params[:message_body],message_type_id:params[:message_type_id])
    if (params[:base64_content].present?) # a_variable is the variable we want to compare
      save_message_attachment(message,params[:base64_content],params[:ext])
    end

    chat_key=generate_chat_key(params[:user_id],params[:reciever_id])
    reciever_message=UserMessage.new(user_id:params[:user_id],message_id:message.id,chat_key:chat_key,sender_id:params[:user_id],reciever_id:params[:reciever_id])
    sender_message=UserMessage.new(user_id:params[:reciever_id],message_id:message.id,chat_key:chat_key,sender_id:params[:user_id],reciever_id:params[:reciever_id])
    
    if reciever_message.save && sender_message.save
      to_user=User.find(params[:reciever_id])
      add_unreaded_message(to_user.id,chat_key)
      notify_user(to_user,"New Message",
      "You Got A New Message From #{@current_user.name}" ,@current_user,"from_messages") if to_user.id != @current_user.id
  
      render json: message, status: :created, location: message
    else
      render json:  reciever_message.errors , status: :unprocessable_entity
    end
  end

  # PATCH/PUT /messages/1
  def update
    if @message.update(message_params)
      render json: @message
    else
      render json: @message.errors, status: :unprocessable_entity
    end
  end

  # DELETE /messages/1
  def destroy
    message=UserMessage.find(params[:id])
    message.destroy
    render json: {result:"true"}
  end

  def destroy_all_chat
    messages=UserMessage.where(user_id:@current_user.id,chat_key:generate_chat_key(@current_user.id,params[:with_user_id]))
    messages.delete_all
    render json: {result:"true"}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def message_params
      params.require(:message).permit(:chat_key, :user_id, :message_body,:reciever_id,:base64_content,:ext,:message_type_id)
    end

    def generate_chat_key(user1_id,user2_id)

      "#{[ Integer(user1_id), Integer(user2_id) ].min}_#{[Integer(user1_id), Integer(user2_id)].max}"
    end

    def make_chat_read(user_id,chat_key)
      read=NewMessageFromChat.find_by(user_id:user_id,chat_key:chat_key)
      if read
        read.destroy
      end
    end

    def add_unreaded_message(user_id,chat_key)
      read=NewMessageFromChat.find_by(user_id:user_id,chat_key:chat_key)
      if read
        read.new_message_number = read.new_message_number + 1
        read.save
      else
        read=NewMessageFromChat.new(user_id:user_id,chat_key:chat_key,new_message_number:1)
        read.save
      end

    end
end
