class JobsController < ApplicationController
  include FilterHelper

  before_action :set_job, only: [:show, :update, :destroy]

  # GET /jobs
  def index
      field_ids= params[:field_ids].blank? ?  Field.all.map(&:id) : params[:field_ids]
      job_type_ids= params[:job_type_ids].blank? ?  JobType.all.map(&:id) : params[:job_type_ids]


    @jobs = Job.where(job_type_id:job_type_ids)
    Job.inject_curent_user(@current_user)
    render json: @jobs,methods: [:intersted_no,:user_info,:while_ago,:current_state,:is_applied,:is_saved]
  end

  def my_created_jobs
    @jobs = Job.where(user_id:@current_user.id)
    Job.inject_curent_user(@current_user)
    render json: @jobs,methods: [:intersted_no,:user_info,:while_ago,:current_state,:is_applied,:is_saved]
  end

  def my_applied_jobs
    if ! @current_user.application.present? 
      render json: []
      return
    end

    @jobs = Job.where(id:JobApplication.where(application_id:@current_user.application.id).map(&:job_id))
    Job.inject_curent_user(@current_user)
    render json: @jobs,methods: [:intersted_no,:user_info,:while_ago,:current_state,:is_saved]
  end

  def my_saved_jobs
    @jobs = Job.where(id:SavedJob.where(user_id:@current_user.id).map(&:job_id))
    Job.inject_curent_user(@current_user)
    render json: @jobs,methods: [:intersted_no,:user_info,:while_ago,:current_state,:is_applied]
  end

  # GET /jobs/1
  def show
    Job.inject_curent_user(@current_user)

    render json: @job,methods: [:intersted_no,:user_info,:while_ago,:is_applied,:is_saved,:job_fields_names]
  end

  # POST /jobs
  def create
    @job = Job.new(job_params)
    @job.user_id = @current_user.id
    if @job.save
      params[:fields_ids].map { |id|  
        job_field=JobField.create(job_id:@job.id,field_id:id)
      }
      render json: @job, status: :created, location: @job
    else
      render json: @job.errors, status: :unprocessable_entity
    end
  end

  def interest_job
    interest=JobInterstedUser.find_by(user_id:@current_user.id,job_id:params[:job_id])
    job=Job.find(params[:job_id])
    if ! interest.present? 
      interest=JobInterstedUser.new(user_id:@current_user.id,job_id:params[:job_id])
      unless interest.save
        render json: interest.errors ,status: "422"
        return
      end

    end
    Job.inject_curent_user(@current_user)
    

    render json: job,methods:[:intersted_no,:current_state]

  end


  def uninterest_job
    interest=JobInterstedUser.find_by(user_id:@current_user.id,job_id:params[:job_id])
    job=Job.find(params[:job_id])
    Job.inject_curent_user(@current_user)

    if  interest.present? 

      interest.destroy

    end

    render json: job,methods:[:intersted_no,:current_state]

  end

  def request_job
    if @current_user.application.nil?
      render json:{error:"no application found"} , status: "404"
      return
    end

    job_application=JobApplication.find_by(job_id:params[:job_id],application_id:@current_user.application.id)
    if  ! job_application.present? 
      job_application=JobApplication.new(job_id:params[:job_id],application_id:@current_user.application.id)
      unless job_application.save
        render json: interest.errors ,status: "422"
        return
      end
    end
    render json: job_application
  end

  def save_job
    if ! SavedJob.find_by(job_id:params[:job_id],user_id:@current_user.id).present?
      SavedJob.create(job_id:params[:job_id],user_id:@current_user.id) 
    end

  end

  def open_job
    job=Job.find(params[:job_id])
    job.is_closed="n"
    if job.save
      render json: job
    else
      render json: job.errors
    end
  end

  def close_job
    job=Job.find(params[:job_id])
    job.is_closed="y"
    if job.save
      render json: job
    else
      render json: job.errors
    end
  end


  # PATCH/PUT /jobs/1
  def update
    if @job.update(job_params)
      render json: @job
    else
      render json: @job.errors, status: :unprocessable_entity
    end
  end

  # DELETE /jobs/1
  def destroy
    @job.destroy
    render json: {result:"true"}
  end

  def top_5_jobs
    job_ids=JobInterstedUser.group(:job_id).select(:job_id).order("count(*) desc").limit(5).map(&:job_id)
    jobs = Job.where(id:job_ids)
    Job.inject_curent_user(@current_user)
    render json: jobs,methods: [:intersted_no,:user_info,:while_ago,:current_state]

  end

  def list_job_to_admin
    date_range=get_date_range(params[:start_date],params[:end_date])
    page= params[:page].present? ? params[:page] : 1

    # render json: Job.where(created_at:date_range).page(page),methods:[:intersted_no,:user_info,:while_ago]
    render json: {
            data:JSON.parse(Job.where(created_at:date_range).page(page).to_json(:methods =>[:user_info,:while_ago,:intersted_no] )),
            items_no:Job.where(created_at:date_range).count
        }

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def job_params
      params.require(:job).permit(:user_id, :job_title, :job_desc,:duties,:phone_no,:extra_phone_no,:email_address,:website,:job_type_id,:latitude,:longitude,:address_desc,:fields_ids)
    end
end
