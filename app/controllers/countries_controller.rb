class CountriesController < ApplicationController
  before_action :set_country, only: [:show, :update, :destroy]
  skip_before_action :authenticate_request, only: [:index]

  # GET /countries
  def index
    # xlsx = Roo::Excelx.new(File.expand_path('./c.xlsx'))
		# xlsx.each_row_streaming(offset: 1) do |row|

		# 	if row[0].value
		# 		Country.create(country_name:row[0].value )
		# 	end
		# end
		# render json: "success"

    @countries = Country.all

    render json: @countries
  end

  # GET /countries/1
  def show
    render json: @country
  end

  # POST /countries
  def create
    @country = Country.new(country_params)

    if @country.save
      render json: @country, status: :created, location: @country
    else
      render json: @country.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /countries/1
  def update
    if @country.update(country_params)
      render json: @country
    else
      render json: @country.errors, status: :unprocessable_entity
    end
  end

  # DELETE /countries/1
  def destroy
    @country.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def country_params
      params.require(:country).permit(:country_name)
    end
end
