class FollowersController < ApplicationController
    include NotificationHelper
    def follow 
        follow=Follower.find_by(user_id:@current_user.id,follower_id:params[:follower_id])
        if follow
            render json: follow
            return
        end

        follow=Follower.new(user_id:@current_user.id,follower_id:params[:follower_id])
        if follow.save
            to_user=User.find(params[:follower_id])
            notify_user(to_user,"New Follower !!",
            "#{@current_user.name} Has Followed You" ,@current_user,"from_users",@current_user.id)
            render json: follow, status: :created
        else
            render json: follow.errors, status: :unprocessable_entity
        end
    end

    def unfollow 
        follow=Follower.find_by(user_id:@current_user.id,follower_id:params[:follower_id])
        if follow
            if ! follow.destroy
                render json: follow.errors, status: :unprocessable_entity
            end
        end
        render json:{result:"true"}
        return
    end

    def test_noti
        res=send_notification(["cX9iypibZEvxq8hs1Ly0kO:APA91bGgNeLdjYV5PlGK8WjxsSmi2UzDdxpPlWMQcCTxR4Cu9V0qnR5DEoBk4LLPmZ82bpC5loXwzXoqd73cdO4VeOTwpZ1Yr5mhH072nvDjAR351WMfyeRupORUzTcnfouQSMiDnLV0"],"test title","msa mas ya madoodaaa")
        # res=UsermailerMailer.welcome_email(@current_user).deliver

        render json: {result:  res}
    end

    def list_my_followers
        # Follower.current_profile_id = @current_user.id
        followers=Follower.joins(:user).where(followers:{follower_id:@current_user.id}).where("name like ?","#{params[:query]}%")
        .select("users.id,users.first_name,users.last_name,users.email,users.job_title,users.profile_img,users.gender,followers.user_id,users.user_profile_image_id,users.user_cover_image_id")
        .map { |follower|
            follower.is_following = Follower.find_by(user_id: @current_user.id,follower_id:follower.id).present? ? true : false
            follower.get_profile_path =  User.find(follower.user_id).get_profile_path
            follower
        }

        render json: followers  , methods: [:is_following, :get_profile_path]
    end

    def list_user_followers
        # Follower.current_profile_id = @current_user.id
        followers=Follower.joins(:user).where(followers:{follower_id:params[:user_id]})
        .select("users.id,users.first_name,users.last_name,users.email,users.job_title,users.profile_img,users.gender,followers.user_id,users.user_profile_image_id,users.user_cover_image_id")
        .map { |follower|
            follower.is_following = Follower.find_by(user_id: params[:user_id],follower_id:follower.id).present? ? true : false
            follower.get_profile_path =  User.find(follower.user_id).get_profile_path
            follower
        }

        render json: followers , methods: [:is_following, :get_profile_path]
    end

    def list_my_following_users
        render json: User.where(id: Follower.where(followers:{user_id:@current_user.id})
                        .map(&:follower_id))
                        .select("users.id,users.first_name,users.last_name,users.email,users.job_title,users.profile_img,users.gender,users.user_profile_image_id,users.user_cover_image_id"), methods: [:get_profile_path]
                        
    end

    def list_user_following_users
    
        render json: User.where(id: Follower.where(followers:{user_id:params[:user_id]})
        .map(&:follower_id))
        .select("users.id,users.first_name,users.last_name,users.email,users.job_title,users.profile_img,users.gender,users.user_profile_image_id,users.user_cover_image_id"), methods: [:get_profile_path]

    end
    
    def block
        block=BlockedUser.find_by(user_id:@current_user.id ,blocked_user_id:params[:blocked_user_id])
        if block
            render json:{result:"true"}
            return
        end


        block=BlockedUser.new(user_id:@current_user.id ,blocked_user_id:params[:blocked_user_id])
        if block.save
            delete_follow_and_follow_back(@current_user.id,params[:blocked_user_id])
            render json:{result:"true"}
            return
        else
            render block.errors,status: 422
        end
    end

    def unblock
        block=BlockedUser.find_by(user_id:@current_user.id ,blocked_user_id:params[:blocked_user_id])
        if block
            block.destroy
        end


        render json:{result:"true"}
    end

    private

    def delete_follow_and_follow_back(user_1_id,user_2_id)
        follows=Follower.where(user_id:user_1_id,follower_id:user_2_id).or(Follower.where(user_id:user_2_id,follower_id:user_1_id))
        follows.delete_all if follows.count > 0
    end

end
