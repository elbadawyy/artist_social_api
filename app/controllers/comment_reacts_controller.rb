class CommentReactsController < ApplicationController
  before_action :set_comment_react, only: [:show, :update, :destroy]

  # GET /comment_reacts
  def index
    @comment_reacts = CommentReact.all

    render json: @comment_reacts
  end

  # GET /comment_reacts/1
  def show
    render json: @comment_react
  end

  # POST /comment_reacts
  def create
    if CommentReact.where(user_id:@current_user.id,react_id:params[:react_id],comment_id:params[:comment_id]).count == 0
      @comment_react = CommentReact.new(comment_react_params)
      @comment_react.user_id=@current_user.id
      if @comment_react.save
        render json: @comment_react, status: :created, location: @comment_react
      else
        render json: @comment_react.errors, status: :unprocessable_entity
      end
    else
      render json: CommentReact.find_by(user_id:@current_user.id,react_id:params[:react_id],comment_id:params[:comment_id])
    end
  end

  def unreact
    comment_react=CommentReact.find_by(user_id:@current_user.id,comment_id:params[:comment_id])
    if comment_react
      comment_react.destroy

    end

  end

  # PATCH/PUT /comment_reacts/1
  def update
    if @comment_react.update(comment_react_params)
      render json: @comment_react
    else
      render json: @comment_react.errors, status: :unprocessable_entity
    end
  end

  # DELETE /comment_reacts/1
  def destroy
    @comment_react.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment_react
      @comment_react = CommentReact.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comment_react_params
      params.require(:comment_react).permit(:user_id, :comment_id, :react_id)
    end
end
