class ProjectsController < ApplicationController
  include ProjectHelper
  include FilterHelper

  before_action :set_project, only: [:show, :update, :destroy]

  # GET /projects
  def index
    @projects = Project.all

    render json: @projects.includes(:project_tags), methods: [:while_ago,:user_info]
  end

  def explore_projects
    country_ids= params[:country_id].blank? ?  Country.all.map(&:id) : params[:country_id]
    field_ids=params[:field_id].blank? ?  params[:main_field_id].blank? ?   Field.all.map(&:id) : Field.where(main_field_id: params[:main_field_id] ).map(&:id)  : params[:field_id]

    projects = Project.joins([:user,:project_fields]).where.not(users:{id:BlockedUser.where(user_id:@current_user.id).map(&:blocked_user_id)})
    .where(users:{country_id: country_ids}).where(project_fields:{field_id: field_ids})
    .where('users.name LIKE ? OR users.job_title LIKE ? OR projects.project_title LIKE ?', "%#{params[:query]}%", "%#{params[:query]}%", "%#{params[:query]}%")
    .distinct.map{ |project|
      project.is_following = Follower.find_by(user_id: @current_user.id,follower_id:project.user.id).present? ? true : false
      project
    }

    render json:projects, methods: [:while_ago,:user_info,:is_following]
  end

  def my_projects
    @projects = Project.where(user_id: @current_user.id)

    render json: @projects, methods: [:while_ago,:user_info]
  end

  # GET /projects
  def user_projects
    @projects = Project.where(user_id: params[:user_id])

    render json: @projects, methods: [:while_ago,:user_info], include: [:project_tags => {:only => [:tag_title] }]
  end

  def project_timeline
    ids=Follower.where(user_id:@current_user.id).map(&:follower_id)
    ids << @current_user.id
    @projects = Project.where(user_id: ids)

    render json: @projects, include: [:project_tags] , methods: [:while_ago,:user_info]
  end
  
  # GET /projects/1
  def show
    render json: @project , include: [:project_images,:project_tags],methods: [:project_field_names,:user_info,:main_field]
  end

  # POST /projects
  def create
    @project = Project.new(project_params)
    @project.user_id = @current_user.id
    if @project.save
      save_project_cover(@project,params["project_cover_base64"],params["ext"]) if  params["project_cover_base64"].present?
      save_project_gallary(@project,params["project_images_base64"]) if  params["project_images_base64"].present?
      save_project_tags(@project,params["project_tags"]) if  params["project_tags"].present?
      save_project_field(@project,params["field_ids"]) if  params["field_ids"].present?
      render json: @project, status: :created, location: @project
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  def upload_project_image
    project=Project.find(params[:project_id])
    save_project_gallary(project,params["project_images_base64"])
    # render json: project.includes(:project_images)
    render json: {result:"true"}
  end

  def delete_image_from_project
    image=ProjectImage.find(params[:image_id])
    image.destroy
  end

  # PATCH/PUT /projects/1
  def update
    if @project.update(project_params)
      save_project_cover(@project,params["project_cover_base64"],params["ext"]) if  params["project_cover_base64"].present?
      save_project_tags(@project,params["project_tags"]) if  params["project_tags"].present?
      save_project_field(@project,params["field_ids"]) if  params["field_ids"].present?

      render json: @project
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  # DELETE /projects/1
  def destroy
    @project.destroy
    render json: {result:"true"}
  end

  def list_project_to_admin
    page= params[:page].present? ? params[:page] : 1

    @projects = Project.page(page)

    # render json: @projects, include: [:project_tags] , methods: [:while_ago,:user_info]

    render json: {
            data:JSON.parse(@projects.page(page).to_json(:methods =>[:user_info,:while_ago] )),
            items_no: @projects.count
        }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def project_params
      params.require(:project).permit(:project_title,:project_images_base64, :description, :cover_path, :project_link,:project_cover_base64)
    end

    def save_project_tags(project,project_tags)
      project.project_tags=Array.new
      project.save
      project_tags.map{ |tag|
        ProjectTag.create(tag_title:tag,project_id:project.id)
      }
    end

    def save_project_field(project,project_field_ids)
      project.project_fields=Array.new
      project.save
      project_field_ids.map{ |field_id|
        ProjectField.create(field_id:field_id,project_id:project.id)
      }
    end

end
