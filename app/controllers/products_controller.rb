class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]

  # GET /products
  def index
    @products = Product.all

    render json: @products
  end

  def download
    send_file("#{Rails.root}/log/development.log")
  end


  
  def list_my_products
    @products = @current_user.products
    render json: @products
  end

  # GET /products/1
  def show
    user_info = @product.user.attributes.slice('name', 'email' , 'profile_img')
    data = {product: @product , images:  @product.product_images , user_info: user_info}
    render json: data
  end

  # POST /products
  def create
    @product = @current_user.products.new(product_params.except( :base64_main_image ,:main_extension , :base64_images_with_ext))
    if @product.save
      @product.update(main_image_url: Product.convert_images(product_params["base64_main_image"] , product_params["main_extension"]))
      product_params["base64_images_with_ext"].each do |image|
        @product.product_images.create(image_url: Product.convert_images(image["images_content"] , image["ex"]))
      end
      render json: @product, status: :created, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params.except(:other_images_url))
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.permit(:product_name, :product_details, :price, :country_id , :currency , :phone , :email , :location, :base64_main_image ,:main_extension , base64_images_with_ext: [:images_content , :ex])
    end
end


