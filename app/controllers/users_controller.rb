class UsersController < ApplicationController
    def user_profile
        user=User.joins("LEFT JOIN countries ON countries.id = users.country_id")
            .select("users.*,countries.country_name").find(params[:user_id])
        user.is_following = Follower.find_by(user_id: @current_user.id,follower_id:user.id).present? ? true : false
        render json:user, methods: [:skills,:user_fields_names,:skills,:followers_number,:following_number,:is_following,:get_profile_path,:get_cover_path],except: [:password_digest]
    end

    def current_user
        render json: @current_user,except: [:password_digest],methods: [:get_profile_path,:get_cover_path]
    end

    def list_users
        users=User.where.not(id:@current_user.id).map { |follower|
            follower.is_following = Follower.find_by(user_id: @current_user.id,follower_id:follower.id).present? ? true : false
            follower
        }
        render json: users,except: [:password_digest],methods: [:is_following,:get_profile_path]
    end

    def total_user_count
        render json: User.all.count
    end

    def last_week_users_count
        render json: User.where(created_at: Date.today - 7.days .. Date.today).count

    end

    def users_by_field
        render json: User.joins(user_fields: :field)
                        .group(:field_id,"user_fields.field_id")
                        .select("COUNT(users.id) AS users_no ,fields.field_name")
    end

    def users_by_country
        render json: User.joins(:country)
        .group(:country_id,"users.country_id")
        .select("COUNT(users.id) AS users_no ,countries.country_name")

    end

    def explore_users
        country_ids= params[:country_id].blank? ?  Country.all.map(&:id) : params[:country_id]
        field_ids=params[:field_id].blank? ?  params[:main_field_id].blank? ?   Field.all.map(&:id) : Field.where(main_field_id: params[:main_field_id] ).map(&:id)  : params[:field_id]

        users=User.joins(:user_fields).where.not(id:@current_user.id).where.not(id:BlockedUser.where(user_id:@current_user.id).map(&:blocked_user_id))
        .where(country_id: country_ids).where(user_fields:{field_id: field_ids})
                .where('name LIKE ? OR job_title LIKE ?', "%#{params[:query]}%", "%#{params[:query]}%").distinct
                .map { |follower|
            follower.is_following = Follower.find_by(user_id: @current_user.id,follower_id:follower.id).present? ? true : false
            follower
        }
    
        render json: users,except: [:password_digest],methods: [:is_following,:get_profile_path]
    end

    def list_users_to_admin
        page= params[:page].present? ? params[:page] : 1

        users=User.where('name LIKE ? OR job_title LIKE ?', "%#{params[:query]}%", "%#{params[:query]}%").page(page)
            #    render json: users
        render json: {
            data:JSON.parse(users.to_json(:methods =>[:get_profile_path] )),
            items_no: User.where('name LIKE ? OR job_title LIKE ?', "%#{params[:query]}%", "%#{params[:query]}%").count
        }

    end

    def list_notifications
        render json: Notification.find(to_id:@current_user.id),methods: [:from_user_info]
    end

    def list_blocked_users
        render json: User.where(id: BlockedUser.where(blocked_users:{user_id:@current_user.id}).map(&:blocked_user_id) )
        .select("users.id,users.first_name,users.last_name,users.email,users.job_title,users.profile_img,users.gender,users.user_profile_image_id,users.user_cover_image_id"), methods: [:get_profile_path]
    end

    def ban_user
        user=User.find(params[:user_id])
        user.is_blocked = "y"

        user.save
    end

    def unban_user
        user=User.find(params[:user_id])
        user.is_blocked = "n"

        user.save
    end

    def delete_user
        user=User.find(params[:user_id])
        Follower.where(user_id:user.id).destroy_all
        Follower.where(follower_id:user.id).destroy_all
        BlockedUser.where(blocked_user_id:user.id).destroy_all
        BlockedUser.where(user_id:user.id).destroy_all
        user.destroy
    end

    def get_pre_assigned_url

        #credentials below for the IAM user I am using
        s3 = Aws::S3::Client.new(
          region:               'us-west-1', #or any other region
          access_key_id:        'AKIAVA2AXO2H4YWDSSK2',
          secret_access_key:    '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',
          signature_version: 'v4'
        )

        signer = Aws::S3::Presigner.new(client: s3)
        url = signer.presigned_url(
          :put_object,
          bucket: 'isyncart-bucket',
          key: "13.txt",
          expires: 3000,
          acl: 'public-read'

          
        )

        render json: {url:url}

    end
end
