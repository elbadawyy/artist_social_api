class ApplicationsController < ApplicationController
  include ApplicationHelper

  before_action :set_application, only: [:show, :update, :destroy]

  # GET /applications
  def index
    @applications = Application.all

    render json: @applications
  end

  def get_application_in_job
    applications=Application.where(id: JobApplication.where(job_id:params[:job_id]).map(&:application_id) )
    applications= applications.map{|application|
      application.is_marked= JobMarkedApplication.find_by(job_id:params[:job_id]).present? ? "y" : "n"
      application
    }
    render json: applications , methods: [:is_marked,:user_info]
  end

  def get_marked_applications
    render json:Application.where(id: JobMarkedApplication.where(job_id:params[:job_id]).map(&:application_id) ), methods: [:user_info]
  end

  # GET /applications/1
  def show
    render json: @application , methods: [:user_info]
  end

  def view_my_application
    if @current_user.application.present?
      render json:  @current_user.application
    else
      render json: {}
    end
  end 

  # POST /applications
  def create
    if @current_user.application.present?
      @application=@current_user.application
      Application.find(@application.id).update(application_params.except(:cv_path,:cv_base64_string,:cv_extension))
      @application.cv_path=save_cv_file(@current_user.id,params[:cv_base64_string],params[:cv_extension])

    else
      @application = Application.new(application_params.except(:cv_path,:cv_base64_string,:cv_extension))
      @application.user_id = @current_user.id
      @application.cv_path=save_cv_file(@current_user.id,params[:cv_base64_string],params[:cv_extension])
    end

    if @application.save
      @current_user.application_filled="y"
      @current_user.save
      render json: @application, status: :created, location: @application
    else
      render json: @application.errors, status: :unprocessable_entity
    end
  end

  # POST /applications
  def mark_application
    if ( ! JobMarkedApplication.find_by(job_id:params[:job_id],application_id:params[:application_id]).present? )
          JobMarkedApplication.create(job_id:params[:job_id],application_id:params[:application_id])

    end

  end

  def unmark_application
    if ( JobMarkedApplication.find_by(job_id:params[:job_id],application_id:params[:application_id]).present? )
          JobMarkedApplication.find_by(job_id:params[:job_id],application_id:params[:application_id]).destroy
    end

  end

  # PATCH/PUT /applications/1
  def update
    if @application.update(application_params.except(:cv_path,:cv_base64_string,:cv_extension))
      Application.find(@application.id).update(application_params.except(:cv_path,:cv_base64_string,:cv_extension)) 
       @application.cv_path=save_cv_file(@current_user.id,params[:cv_base64_string],params[:cv_extension]) if application_params["cv_base64_string"]
      render json: @application
    else
      render json: @application.errors, status: :unprocessable_entity
    end
  end

  # DELETE /applications/1
  def destroy
    @application.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_application
      @application = Application.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def application_params
      params.permit(:user_id, :name, :phone_no, :another_phone_no, :email, :latitude, :longitude, :work_experience, :education_details, :portofolio_link, :cv_path,:cv_base64_string,:cv_extension , :adress)
    end
end
