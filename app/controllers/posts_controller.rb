class PostsController < ApplicationController
    include PostHelper
    include NotificationHelper
    include FilterHelper

    def list_my_posts
        render json: @current_user.posts
    end

    def create_post
        event_id= params[:event_id].present? ? params[:event_id] : nil
        post_location_id= params[:event_id].present? ? 2 : 1

        post=Post.new(user_id:@current_user.id,post_body:params[:post_body],
        post_type_id:params[:post_type_id],is_shareable:params[:is_shareable],is_commentable:params[:is_commentable],
        post_body_html:params[:post_body_html],post_location_id:post_location_id,event_id:event_id)

        if post.save
            if params[:post_tag_ids].present?
                params[:post_tag_ids].map { |user_id|
                    to_user=User.find(user_id)
                    notify_user(to_user,"New Tag !!",
                    "#{@current_user.name} Has Tagged You On Post" ,@current_user,"from_posts",post.id) if to_user.id != @current_user.id
                    save_post_tag(post.id,params[:post_tag_ids])


                }
            end
            save_post_hashtag(post.id,params[:hashtags])
            if (params[:base64_content].present?) # a_variable is the variable we want to compare
                save_post_attachment(post,params[:base64_content],params[:extension])
            end

            render json: post, methods: [:attachment_path],status: "200"
        else
            render json: post.errors, status: "422"
        end
    end

    def edit_privacy
        post=Post.find(params[:post_id])
        if params[:is_commentable] != ""
            post.is_commentable=params[:is_commentable]
        end

        if params[:is_shareable] != ""
            post.is_shareable=params[:is_shareable]

        end
        if post.save
            render json: post
            

        else

            render json: post.errors,status:"422"
        end
    end

    def edit_post
        post=Post.find(params[:post_id])
        post.update(user_id:@current_user.id,post_body:params[:post_body],
        post_type_id:params[:post_type_id])

        if post.save
            if (params[:base64_content].present?) # a_variable is the variable we want to compare
                save_post_attachment(post,params[:base64_content],params[:extension])
            end

            render json: post, methods: [:attachment_path], status: "200"
        else
            render json: post.errors, status: "422"
        end
    end

    def delete_post
        post=Post.find(params[:post_id])
        post.destroy
        render json: {result:"true"}
    end

    def react
        post=PostReact.find_by(user_id:@current_user.id,post_id:params[:post_id])
        Post.inject_curent_user(@current_user)

        if post
            post.react_id = params[:react_id]
            post.save
            to_user=post.post.user
            notify_user(to_user,"New React !!",
            "#{@current_user.name} Has Reacted On Your Post" ,@current_user,"from_posts",post.post.id) if to_user.id != @current_user.id


            render json:post.post, methods: [:while_ago,:reacts,:shares_no,:comments_no,:current_react]
        else
            post=PostReact.new(user_id:@current_user.id,post_id:params[:post_id],react_id: params[:react_id])
            if post.save
                to_user=post.post.user
                notify_user(to_user,"New React !!",
                "#{@current_user.name} Has Reacted On Your Post" ,@current_user,"from_posts",post.post.id) if to_user.id != @current_user.id

                render json:post.post, methods: [:while_ago,:reacts,:shares_no,:comments_no,:current_react]
            else
                render json:post.post, methods: [:while_ago,:reacts,:shares_no,:comments_no,:current_react]
            end
        end


    end

    def unreact
        Post.inject_curent_user(@current_user)

        post=PostReact.find_by(user_id:@current_user.id,post_id:params[:post_id])
        if post
            post.destroy
        end
        render json:post.post, methods: [:while_ago,:reacts,:shares_no,:comments_no,:current_react]
    end

    def share_post
        post=Post.new(user_id:@current_user.id,shared_from_id: params[:original_post_id],post_body:params[:post_body])
        
        if post.save
            
            to_user=post.user
            notify_user(to_user,"New Share !!",
            "#{@current_user.name} Has Shared Your Post" ,@current_user ,"from_posts") if to_user.id != @current_user.id

            render json: post , methods: [:while_ago,:reacts,:shares_no,:comments_no], status: "200"
        else
            render json: post.errors, status: "422"
        end
    end

    def list_post_to_admin
        date_range=get_date_range(params[:start_date],params[:end_date])
        page= params[:page].present? ? params[:page] : 1
        render json: {
            data:JSON.parse(Post.where(posts:{created_at: date_range,shared_from_id: nil}).page(page).to_json(:methods =>[:user_info,:while_ago,:attachment_path] )),
            items_no:Post.where(posts:{created_at: date_range,shared_from_id: nil}).count,
            page_no:Float(Post.where(posts:{created_at: date_range,shared_from_id: nil}).count / 25).floor
        }
        #  ,methods: [:user_info,:items_no]
    end
end
