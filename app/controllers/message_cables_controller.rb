class MessageCablesController < ActionController::API

    def messages
      messages = Conversation.find_by(room_code: params[:room_id]).message_cables.paginate(page: params[:page])
      render json: messages
    end

    def room_id
      sender= params[:sender_id].to_s
      receiver = params[:reciever_id].to_s
      room = Conversation.room_id(sender ,receiver)
      render json: {room_id: room}
    end

    def my_latest_messages
      user = User.find(params[:user_id])
      excludes=[]
      messages=[]
      User.first.received_messages.limit(100).each do |m|
        if excludes.include? m.conversation_id
          next
        else
          messages << m
          excludes << m.conversation_id
        end
      end
      latest_messages=[]
      messages.each do |message|
        user = User.find(message.sender_id)
        latest_messages << {user: user.id  , content: message.content , user_name: user.name , time: message.created_at  , image_path: user.user_profile_image.image_path , type: message.base64_type }
      end

      render json: latest_messages
      
    end
    

end   
