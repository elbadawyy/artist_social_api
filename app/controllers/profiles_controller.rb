class ProfilesController < ApplicationController
    # def edit_all_info
    #     @current_user.update_all()
    # end
    include ProfileHelper

    def update_skills
        skills_array=Array.new
        params[:skills].map { |skill_string|
            skill=UserSkill.new(skill_title:skill_string)
            skills_array << skill
        }
        @current_user.user_skills = skills_array
        if @current_user.save
            render json: @current_user
        else
            render json: @current_user.errors , status: 422
        end
    end

    def update_about
        @current_user.about_me = params[:about_me]
        if @current_user.save
            render json: @current_user
        else
            render json: @current_user.errors , status: 422
        end
    end

    # for mobile
    def add_skill
        skill=UserSkill.new(skill_title:params[:skill_string])
        @current_user.user_skills << skill
        if @current_user.save
            render json: @current_user
        else
            render json: @current_user.errors , status: 422
        end

    end

    #for mobile
    def delete_skill
        skill=UserSkill.find(params[:skill_id])
        skill.delete if skill
    end

    def upload_profile_image
        if(params[:base64_string].present? && params[:extension].present?)
            path=save_profile_image(@current_user.id,params[:base64_string],params[:extension])
            profile=UserProfileImage.create(user_id:@current_user.id,image_path:path)
            @current_user.user_profile_image_id=profile.id
            if @current_user.save
                render json: {path: path},status: 200
            else
                render json: {error: @current_user.errors}

            end
        else
            render json: {error: "missing paramters"} ,status: 422
        end
    end

    def upload_cover_image
        if(params[:base64_string].present? && params[:extension].present?)
            path=save_cover_image(@current_user.id,params[:base64_string],params[:extension])
            cover=UserCoverImage.create(user_id:@current_user.id,cover_path:path)
            @current_user.user_cover_image_id=cover.id
            if @current_user.save
                render json: {path: path},status: 200
            else
                render json: {error: @current_user.errors}

            end
        else
            render json: {error: "missing paramters"} ,status: 422
        end
    end

    #mobile
    def edit_all_profile_info
        @current_user.update(email: params[:email],  first_name:params[:first_name],last_name:params[:last_name],date_of_birth:params[:date_of_birth],
            gender:params[:gender],country_id:params[:country_id],mobile_num:params[:mobile_num],
            job_title:params[:job_title],about_me:params[:about_me],add_call_allowed:params[:add_call_allowed])
        
        
        @current_user.update(name: params[:first_name] + " " + params[:last_name])
        @current_user.user_fields=[]
        @current_user.user_skills=[]
            if @current_user.save
            params[:field_ids].map { |field_id|
			    UserField.create(user_id:@current_user.id,field_id:field_id)
            } if params[:field_ids].present?
            
            params[:skills].map { |title|
                UserSkill.create(user_id:@current_user.id,skill_title:title)
		    } if params[:skills].present?
                render json: {result:"true"}
		else
			render json:@current_user.errors,status: 422
		end
    end

    def get_skills
        user=User.find(params[:user_id])
        render json: user.user_skills
    end
end
