class CommentsController < ApplicationController
  include NotificationHelper

  before_action :set_comment, only: [:show, :update, :destroy]

  # GET /comments
  def index
    @comments = Comment.all

    render json: @comments
  end

  # GET /comments/1
  def show
    render json: @comment
  end

  # POST /comments
  def create
    @comment = Comment.new(comment_params)

    if @comment.save

      to_user=@comment.post.user
      notify_user(to_user,"New Comment !!",
      "#{@current_user.name} Has Commented On Your Post" ,@current_user)

      render json: @comment, status: :created, location: @comment
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /comments/1
  def update
    if @comment.update(comment_params)
      render json: @comment
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /comments/1
  def destroy
    @comment.destroy
    render json: {result:"true"}
  end

  def list_post_comments
    post=Post.find(params[:post_id])
    Comment.inject_curent_user(@current_user)
    comments=Comment.where(post_id:post.id)
    

    render json: comments,:methods => [:commenter_info,:while_ago,:reacts,:replies,:current_react]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:post_id,:user_id ,:comment_body)
    end
end
