class AuthenticationController < ApplicationController
 skip_before_action :authenticate_request,:except => [:add_fcm_token]

	def authenticate
		command = AuthenticateUser.call(params[:email], params[:password])

		if command.success?
			user=User.find_by_email(params[:email])
			render json: { auth_token: command.result,user:user }
		else
			render json: { error: command.errors }, status: :unauthorized
		end
	end

	def sign_up
		user=User.new(email: params[:email] , password: params[:password] , password_confirmation: params[:password],
			first_name:params[:first_name],last_name:params[:last_name],date_of_birth:params[:date_of_birth],gender:params[:gender],
			country_id:params[:country_id],mobile_num:params[:mobile_num])

			user.name=params[:first_name] + " " + params[:last_name]
            user.job_title= Field.find(params[:field_ids][0]).field_name
		if user.save
			command = AuthenticateUser.call(params[:email], params[:password])
			add_fields_to_user user.id , params[:field_ids]
			if command.success?
				render json: { auth_token: command.result,user:user }
			else
				render json: { error: command.errors }, status: :unauthorized
			end

		else
			render json:user.errors,status: 422
		end
	end

	def login_social
		# {id: "4772948446112839", email: "hb_pharo@yahoo.com", name: "Mohamed Madi", picture: {…}}
		social_user=SocialAccount.find_by(account_id:params[:account_id],from:params[:from])
		if social_user
			command = AuthenticateUser.social_user_request(params[:account_id])
			user=social_user.user
			
		else
			user=add_new_social_user(params[:account_id],params[:from],params[:email],params[:name])
			command = AuthenticateUser.social_user_request(params[:email])

		end

		if command
			render json: { auth_token: command,user:user }
		else
			render json: { error: "" }, status: :unauthorized
		end
	end

	def send_reset_password_code
		user=User.find_by_email(params[:email])
		if ! user.present?
			render json: {result:"this email is not registered"},status: 404
			return
		end

		six_digits=(SecureRandom.random_number(9e5) + 1e5).to_i
		ResetPasswordCode.create(user_email:params[:email],code:six_digits)
		res=ResetPasswordMailer.reset_password(user,six_digits).deliver
		render json:{result:"true"}

	end

	def check_password_code
		if (ResetPasswordCode.find_by(user_email: params[:email],code:params[:code]))
			render json:  params[:code]
		else
			render json: false
		end
	end

	def add_fcm_token
		fcm_token=UserFcmToken.find_by(fcm_token:params[:fcm_token])
		if ! fcm_token.present?
			UserFcmToken.create(user_id:@current_user.id,fcm_token:params[:fcm_token])
		else
			fcm_token.user_id=@current_user.id
			fcm_token.save
		end
		render json: {result:"true"}
	end

	def reset_password
		user_code=ResetPasswordCode.find_by(user_email:params[:email],code:params[:code])
		if ! user_code.present?
			render json: {result:"Wrong Code"},status: 401
			return
		end
		user=User.find_by_email(params[:email])
		user.password=params[:new_password]
		if user.save
			render json: user
		else
			render json: user.errors, status: 422

		end
	end

	def logout
		fcm_token=UserFcmToken.find_by(fcm_token:params[:fcm_token])
		fcm_token.destroy if fcm_token.present?
		render json: {result:"true"}
	end

	private

	def add_fields_to_user user_id ,field_ids
		field_ids.map { |field_id|
			UserField.create(user_id:user_id,field_id:field_id)

		}

	end

	def add_new_social_user(account_id,from,email,name)
		user=User.find_by_email(email)
		if user
			social_user=SocialAccount.create(account_id:account_id,from:from,email:email,user_id:user.id)

		else
			pass=SecureRandom.hex

			user=User.create(email:email,password:pass,password_confirmation: pass,name:params[:name],first_name:params[:name].split(" ")[0],last_name:params[:name].split(" ")[1])
			social_user=SocialAccount.create(account_id:account_id,from:from,email:email,user_id:user.id)

		end
		user
	end

end
