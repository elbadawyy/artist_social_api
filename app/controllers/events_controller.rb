class EventsController < ApplicationController
  include FilterHelper
  include EventHelper

  before_action :set_event, only: [:show, :update, :destroy]

  # GET /events
  def index
    @events = Event.all
    Event.inject_curent_user(@current_user)

    render json: @events,methods:[:intersted_going_users,:current_state,:user_info]
  end

  def list_online_events
    @events = Event.where(event_type_id: 1)
    Event.inject_curent_user(@current_user)

    render json: @events,methods:[:intersted_going_users,:current_state,:user_info]
  end

  def list_local_events
    @events = Event.where(event_type_id: 2)
    Event.inject_curent_user(@current_user)

    render json: @events,methods:[:intersted_going_users,:current_state,:user_info]
  end

  def list_my_event
    @events = Event.where(user_id:@current_user.id )
    Event.inject_curent_user(@current_user)

    render json: @events,methods:[:intersted_going_users,:current_state,:user_info]

  end

  # GET /events/1
  def show
    Event.inject_curent_user(@current_user)

    render json: @event,methods:[:intersted_going_users,:current_state,:user_info]
  end

  # POST /events
  def create
    @event = Event.new(event_params)

    @event.user_id=@current_user.id
    if @event.save
      @event.cover_path=save_event_cover(@event.id,params[:base64_string],params[:extension])
      @event.save
      render json: @event, status: :created, location: @event
    else
      render json: @event.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /events/1
  def update
    if @event.update(event_params)
      render json: @event
    else
      render json: @event.errors, status: :unprocessable_entity
    end
  end

  def interest_event
    interest=EventInterstedUser.find_by(user_id:@current_user.id,event_id:params[:event_id])
    event=Event.find(params[:event_id])
    if ! interest.present? 
      interest=EventInterstedUser.new(user_id:@current_user.id,event_id:params[:event_id])
      unless interest.save
        render json: event.errors ,status: "422"
        return
      end

    end
    Event.inject_curent_user(@current_user)
    

    render json: event,methods:[:current_state,:user_info,:intersted_going_users]

  end


  def uninterest_event
    interest=EventInterstedUser.find_by(user_id:@current_user.id,event_id:params[:event_id])
    event=Event.find(params[:event_id])

    Event.inject_curent_user(@current_user)

    if  interest.present? 

      interest.destroy

    end

    render json: event,methods:[:user_info,:current_state,:intersted_going_users]

  end

  def going_event
    going=EventGoingUser.find_by(user_id:@current_user.id,event_id:params[:event_id])
    event=Event.find(params[:event_id])
    if ! going.present? 
      going=EventGoingUser.new(user_id:@current_user.id,event_id:params[:event_id])
      unless going.save
        render json: going.errors ,status: "422"
        return
      end

    end
    Event.inject_curent_user(@current_user)
    

    render json: event,methods:[:current_state,:intersted_going_users,:user_info]

  end


  def ungoing_event
    going=EventGoingUser.find_by(user_id:@current_user.id,event_id:params[:event_id])
    event=Event.find(params[:event_id])

    Event.inject_curent_user(@current_user)

    if  going.present? 

      going.destroy

    end

    render json: event,methods:[:current_state,:intersted_going_users,:user_info]

  end

  def list_event_to_admin
    date_range=get_date_range(params[:start_date],params[:end_date])
    page= params[:page].present? ? params[:page] : 1
    render json: {
            data:JSON.parse(Event.where(created_at:date_range).page(page).to_json(:methods =>[:user_info,:intersted_going_users] )),
            items_no: Event.where(created_at:date_range).count
        }

    # render json: Event.where(created_at:date_range).page(page),methods:[:intersted_going_users,:user_info]
  end



  # DELETE /events/1
  def destroy
    @event.destroy
    render json: {result:"true"}
  end

  def top_5_events
    event_ids=EventInterstedUser.group(:event_id).select(:event_id).order("count(*) desc").limit(5).map(&:event_id)
    events = Event.where(id:event_ids)
    Event.inject_curent_user(@current_user)
    render json: events,methods: [:intersted_going_users,:current_state,:user_info]

  end

  def going_users
    render json:  User.where(id: EventGoingUser.where(event_id:params[:event_id]).map(&:user_id) ).select("id,email,name,job_title,gender,user_profile_image_id"),methods: [:get_profile_path]
  end

  def intersted_users
    render json:  User.where(id: EventInterstedUser.where(event_id:params[:event_id]).map(&:user_id) ).select("id,email,name,job_title,gender,user_profile_image_id"),methods: [:get_profile_path]
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def event_params
      params.require(:event).permit( :event_title, :user_id, :event_desc,:place,:from,:to,:event_type_id,
        :brief,:start_time,:end_time,:event_link,:more_join_info,:is_free,:ticket_price,
        :more_ticket_info,:latitude,:longitude,:location_desc )
    end
end
