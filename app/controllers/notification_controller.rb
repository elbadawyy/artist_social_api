class NotificationController < ApplicationController
    def list_notification
        render json: Notification.where(to_id:@current_user.id,created_at: Time.now - 2.week .. Time.now ).order({ created_at: :desc }), :methods => [:from_user_info,:while_ago]
        # render json: "true"
    end

    def get_unreaded_notifications
        count= UnreadedNotification.find_by_user_id(@current_user.id).present? ? UnreadedNotification.find_by_user_id(@current_user.id).unreaded_count : 0
        render json: {count: count}
    end

    def read_notifications
        unread = UnreadedNotification.find_by_user_id(@current_user.id)

        if unread.present?
            unread.unreaded_count = 0
            unread.save
        end
        render json: {result:"true"}
    end
    
end
