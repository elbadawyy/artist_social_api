class TimelineController < ApplicationController
    def my_posts
        # user_id	post_body	post_type_id
        post_per_once=3
        page= params[:page].present? ? params[:page] : 0
        Post.inject_curent_user(@current_user)
        render json: Post.where(user_id: @current_user.id)
            .order(id: :desc).limit(post_per_once).offset(Integer(page)*Integer(post_per_once)), methods: [:user_info,:while_ago,:attachment_path,:original_post,:reacts,:shares_no,:current_react,:comments_no,:last_comment,:included_hashtags,:included_tags_user_info]
    end

    def post_details
        
        Post.inject_curent_user(@current_user)
        Comment.inject_curent_user(@current_user)
        render json: Post.find(params[:post_id]), 
        methods: [:user_info,:while_ago,:attachment_path,:original_post,:reacts,:shares_no,:current_react,:comments_no,:last_comment,:included_hashtags,:included_tags_user_info]
    end

    def reacts_details
        Post.inject_curent_user(@current_user)
        PostReact.inject_curent_user(@current_user)
        post=Post.find(params[:post_id])
        render json: post.post_reacts_details
    end

    def another_profile_posts
        # user_id	post_body	post_type_id
        if Follower.where(follower_id:params[:user_id],user_id:@current_user.id).count == 0
            render json: [] 
            return 
        end
        post_per_once=3
        page= params[:page].present? ? params[:page] : 0
        render json: Post.where(user_id: params[:user_id])
                        .offset(Integer(page)*Integer(post_per_once)).limit(post_per_once).order(created_at: :desc), methods: [:while_ago,:user_info,:attachment_path,:original_post,:reacts,:shares_no,:current_react,:comments_no,:last_comment,:included_hashtags,:included_tags_user_info]
    end

    def all_posts
        post_per_once=3
        page= params[:page].present? ? params[:page] : 0
        Post.inject_curent_user(@current_user)
        Comment.inject_curent_user(@current_user)

        render json: Post.all
            .order(id: :desc).limit(post_per_once).offset(Integer(page)*Integer(post_per_once)), 
            methods: [:while_ago,:user_info,:attachment_path,:original_post,:reacts,:shares_no,:current_react,:comments_no,:last_comment,:included_hashtags,:included_tags_user_info]
    end

    def all_posts_bak
        post_per_once=3
        page= params[:page].present? ? params[:page] : 0
        Post.inject_curent_user(@current_user)

        render json: Post.where(user_id: [@current_user.id] + Follower.where(user_id:@current_user.id).map(&:follower_id)  )
            .order(id: :desc).limit(post_per_once).offset(Integer(page)*Integer(post_per_once)), 
            methods: [:while_ago,:user_info,:attachment_path,:original_post,:reacts,:shares_no,:current_react,:comments_no,:last_comment,:included_hashtags,:included_tags_user_info]
    end

    def event_posts
        post_per_once=3
        page= params[:page].present? ? params[:page] : 0
        Post.inject_curent_user(@current_user)
        Comment.inject_curent_user(@current_user)

        render json: Post.where(event_id:params[:event_id])
            .order(id: :desc).limit(post_per_once).offset(Integer(page)*Integer(post_per_once)), 
            methods: [:while_ago,:user_info,:attachment_path,:original_post,:reacts,:shares_no,:current_react,:comments_no,:last_comment,:included_hashtags,:included_tags_user_info]
    end


    def hashtag
        post_per_once=3
        page= params[:page].present? ? params[:page] : 0
        Post.inject_curent_user(@current_user)

        render json: Post.joins(post_hashtags: :hashtag).where(hashtags: {hashtag_body: params[:hashtag]})
            .order(id: :desc).limit(post_per_once).offset(Integer(page)*Integer(post_per_once)), 
            methods: [:while_ago,:user_info,:attachment_path,:original_post,:reacts,:shares_no,:current_react,:comments_no,:last_comment,:included_hashtags,:included_tags_user_info]
    end
end
