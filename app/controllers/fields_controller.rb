class FieldsController < ApplicationController
  before_action :set_field, only: [:show, :update, :destroy]
  skip_before_action :authenticate_request, only: [:index,:get_fields,:main_fields]

  # GET /fields
  def index
    @fields = Field.all

    render json: @fields
  end

  def get_fields
    render json: Field.where(main_field_id:params[:main_field_id])
  end

  def main_fields
    render json: MainField.all

  end

  # GET /fields/1
  def show
    render json: @field
  end

  def add_fields_to_main_field
    main=MainField.new(main_title:params[:new_main_field])
    fields=Array.new
    params[:field_ids].map { |field_id|
      field=Field.find(field_id)
      fields << field
    }
    main.fields=fields
    if main.save
      render json: main
    else
      render json: main.errors ,status: "422"
    end

  end

  # POST /fields
  def create
    @field = Field.new(field_params)

    if @field.save
      render json: @field, status: :created, location: @field
    else
      render json: @field.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /fields/1
  def update
    if @field.update(field_params)
      render json: @field
    else
      render json: @field.errors, status: :unprocessable_entity
    end
  end

  # DELETE /fields/1
  def destroy
    @field.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_field
      @field = Field.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def field_params
      params.require(:field).permit(:field_name)
    end
end
