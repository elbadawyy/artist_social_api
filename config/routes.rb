Rails.application.routes.draw do
  
  mount ActionCable.server => '/cable'

  resources :products do
    collection do
      get :list_my_products
    end
    collection do
      get :download
    end
  end
  resources :applications
  resources :comment_replies
  resources :comment_reacts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  
  ############################# authentication #############################
  post 'authenticate', to: 'authentication#authenticate'
  post 'login_social', to: 'authentication#login_social'
  post 'sign_up', to: 'authentication#sign_up'
  post 'send_reset_password_code', to: 'authentication#send_reset_password_code'
  post 'check_password_code', to: 'authentication#check_password_code'
  post 'reset_password', to: 'authentication#reset_password'
  post 'add_fcm_token', to: 'authentication#add_fcm_token'
  post 'logout', to: 'authentication#logout'

  ############################# users #############################
  get 'users/user_profile/:user_id', to: 'users#user_profile'
  get 'users/list_users', to: 'users#list_users'
  get 'users/current_user', to: 'users#current_user'
  get 'users/list_blocked_users', to: 'users#list_blocked_users'
  get 'users/get_pre_assigned_url', to: 'users#get_pre_assigned_url'


  post 'users/explore_users', to: 'users#explore_users'
  
  ############################# profiles #############################
  post 'profiles/update_skills', to: 'profiles#update_skills'
  post 'profiles/add_skill', to: 'profiles#add_skill'
  delete 'profiles/delete_skill/:skill_id', to: 'profiles#delete_skill'
  post 'profiles/update_about', to: 'profiles#update_about'
  post 'profiles/upload_profile_image', to: 'profiles#upload_profile_image'
  post 'profiles/upload_cover_image', to: 'profiles#upload_cover_image'
  
  put 'profiles/edit_all_profile_info', to: 'profiles#edit_all_profile_info'

  get 'profiles/get_skills/:user_id', to: 'profiles#get_skills'

  
  ############################# posts #############################
  get 'posts/list_my_orders', to: 'posts#list_my_orders'

  post 'posts/create_post', to: 'posts#create_post'
  post 'posts/share_post/:original_post_id', to: 'posts#share_post'
  post 'posts/react/:react_id/:post_id', to: 'posts#react'
  post 'posts/unreact/:post_id', to: 'posts#unreact'

  put 'posts/edit_post/:post_id', to: 'posts#edit_post'
  put 'posts/edit_privacy/:post_id', to: 'posts#edit_privacy'

  delete 'posts/delete_post/:post_id', to: 'posts#delete_post'
  ############################# comments #############################
  get 'comments/list_post_comments/:post_id', to: 'comments#list_post_comments'
  ############################# timeline #############################
  get 'timeline/my_posts', to: 'timeline#my_posts'
  get 'timeline/all_posts', to: 'timeline#all_posts'
  get 'timeline/event_posts/:event_id', to: 'timeline#event_posts'
  get 'timeline/another_profile_posts/:user_id', to: 'timeline#another_profile_posts'
  get 'timeline/post_details/:post_id', to: 'timeline#post_details'
  get 'timeline/reacts_details/:post_id', to: 'timeline#reacts_details'

  get 'timeline/hashtag', to: 'timeline#hashtag'
  ############################# messages #############################
  get 'messages/list_messages_with_users', to: 'messages#list_messages_with_users'
  get 'messages/list_message_from_user/:with_user_id', to: 'messages#list_message_from_user'
  get 'messages/get_total_unreaded_message', to: 'messages#get_total_unreaded_message'

  delete 'messages/destroy_all_chat/:with_user_id', to: 'messages#destroy_all_chat'


  ############################# followers #############################
  post 'followers/follow/:follower_id', to: 'followers#follow'
  post 'followers/unfollow/:follower_id', to: 'followers#unfollow'
  post 'followers/block/:blocked_user_id', to: 'followers#block'
  post 'followers/unblock/:blocked_user_id', to: 'followers#unblock'
  get 'followers/list_my_followers', to: 'followers#list_my_followers'
  get 'followers/list_user_followers/:user_id', to: 'followers#list_user_followers'
  get 'followers/list_my_following_users', to: 'followers#list_my_following_users'
  get 'followers/list_user_following_users/:user_id', to: 'followers#list_user_following_users'
  post 'followers/test_noti', to: 'followers#test_noti'

  ############################# projects #############################
  post 'projects/upload_project_image/:project_id', to: 'projects#upload_project_image'
  post 'projects/explore_projects', to: 'projects#explore_projects'

  get 'projects/my_projects', to: 'projects#my_projects'
  get 'projects/user_projects/:user_id', to: 'projects#user_projects'
  get 'projects/project_timeline', to: 'projects#project_timeline'
  delete 'projects/delete_image_from_project/:image_id', to: 'projects#delete_image_from_project'
  
  ############################# jobs #############################
  post 'jobs/interest_job/:job_id', to: 'jobs#interest_job'
  post 'jobs/uninterest_job/:job_id', to: 'jobs#uninterest_job'
  post 'jobs/request_job/:job_id', to: 'jobs#request_job'
  post 'jobs/save_job/:job_id', to: 'jobs#save_job'
  post 'jobs/jobs_with_filter', to: 'jobs#index'

  put 'jobs/open_job/:job_id', to: 'jobs#open_job'
  put 'jobs/close_job/:job_id', to: 'jobs#close_job'

  get 'jobs/top_5_jobs', to: 'jobs#top_5_jobs'
  get 'jobs/my_created_jobs', to: 'jobs#my_created_jobs'
  get 'jobs/my_applied_jobs', to: 'jobs#my_applied_jobs'
  get 'jobs/my_saved_jobs', to: 'jobs#my_saved_jobs'
  
  ############################# events #############################
  post 'events/interest_event/:event_id', to: 'events#interest_event'
  post 'events/uninterest_event/:event_id', to: 'events#uninterest_event'
  post 'events/going_event/:event_id', to: 'events#going_event'
  post 'events/ungoing_event/:event_id', to: 'events#ungoing_event'
  
  get 'events/top_5_events', to: 'events#top_5_events'
  get 'events/list_online_events', to: 'events#list_online_events'
  get 'events/list_local_events', to: 'events#list_local_events'
  get 'events/list_my_event', to: 'events#list_my_event'
  get 'events/going_users/:event_id', to: 'events#going_users'
  get 'events/intersted_users/:event_id', to: 'events#intersted_users'

  ############################# fields #############################
  get 'fields/main_fields', to: 'fields#main_fields'
  get 'fields/get_fields/:main_field_id', to: 'fields#get_fields'

  ############################# fields #############################
  get 'notifications/list_notification', to: 'notification#list_notification'
  get 'notifications/get_unreaded_notifications', to: 'notification#get_unreaded_notifications'
  post 'notifications/read_notifications', to: 'notification#read_notifications'

  ############################# comment reacts #############################
  delete 'comment_reacts/unreact/:comment_id', to: 'comment_reacts#unreact'

  ############################# applications #############################
  post 'applications/mark_application', to: 'applications#mark_application'
  post 'applications/unmark_application', to: 'applications#unmark_application'
  get 'applications/get_application_in_job/:job_id', to: 'applications#get_application_in_job'
  get 'applications/get_marked_applications/:job_id', to: 'applications#get_marked_applications'
  get 'application/view_my_application', to: 'applications#view_my_application'


  ############################# just for us #############################
  # post 'add_fields_to_main_field', to: 'fields#add_fields_to_main_field'


  ########################################################## ADMIN DHASHBOARD ##########################################################

  ############################# users #############################
  put 'users/ban_user/:user_id', to: 'users#ban_user'
  put 'users/unban_user/:user_id', to: 'users#unban_user'
  delete 'users/delete_user/:user_id', to: 'users#delete_user'


  ############################# posts #############################
  post 'posts/list_post_to_admin', to: 'posts#list_post_to_admin'

  ############################# projects #############################
  post 'projects/list_project_to_admin', to: 'projects#list_project_to_admin'

  ############################# events #############################
  post 'events/list_event_to_admin', to: 'events#list_event_to_admin'

  ############################# jobs #############################
  post 'jobs/list_job_to_admin', to: 'jobs#list_job_to_admin'
 
 
  ############################# users #############################

  post 'users/list_users_to_admin', to: 'users#list_users_to_admin'
  
  get 'users/total_user_count', to: 'users#total_user_count'
  get 'users/last_week_users_count', to: 'users#last_week_users_count'
  get 'users/users_by_field', to: 'users#users_by_field'
  get 'users/users_by_country', to: 'users#users_by_country'

  

  resources :projects
  resources :comments
  resources :items
  resources :countries
  resources :fields
  resources :messages
  resources :jobs
  resources :events
  resources :applications
  resources :comment_replies
  resources :comment_reacts

  post 'messages_cable' , to: 'message_cables#messages'
  post 'room_id' , to: 'message_cables#room_id'
  post 'my_latest_messages' , to: 'message_cables#my_latest_messages'
  



end
